// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `configuration.ts` with `configuration.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const apiDomain = 'dev.api.oh-my-quiz.ronasit.com';
const apiURL = `https://${apiDomain}`;

export const configuration = {
  production: false,
  appUrl: 'https://localhost:4200',
  api: {
    url: apiURL,
    whitelisted_domains: [
      apiDomain
    ],
    unauthorized_routes: [
      `${apiURL}/login`,
      `${apiURL}/register`,
      `${apiURL}/auth/forgot-password`,
      `${apiURL}/auth/restore-password`
    ]
  },
  languages: {
    available: ['en'],
    default: 'en'
  },
  notification: {
    timeOut: 5000,
    positionClass: 'toast-bottom-right',
    toastClass: 'ngx-toastr'
  },
  modal: {
    panelClass: 'modal',
    maxWidth: '100vw'
  },
  echo: {
    url: apiURL
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
