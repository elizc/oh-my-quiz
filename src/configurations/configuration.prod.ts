const apiDomain = 'dev.api.oh-my-quiz.ronasit.com';
const apiURL = `https://${apiDomain}`;

export const configuration = {
  production: true,
  appUrl: 'https://dev.oh-my-quiz.ronasit.com',
  api: {
    url: apiURL,
    whitelisted_domains: [
      apiDomain
    ],
    unauthorized_routes: [
      `${apiURL}/login`,
      `${apiURL}/register`,
      `${apiURL}/auth/forgot-password`,
      `${apiURL}/auth/restore-password`
    ]
  },
  languages: {
    available: ['en'],
    default: 'en'
  },
  notification: {
    timeOut: 5000,
    positionClass: 'toast-bottom-right',
    toastClass: 'ngx-toastr'
  },
  modal: {
    panelClass: 'modal',
    maxWidth: '100vw'
  },
  echo: {
    url: apiURL
  }
};
