import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PublicForgotPasswordPageState } from './state';

const selectFeature = (state: AppState) => state.publicForgotPasswordPage;

export class PublicForgotPasswordPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PublicForgotPasswordPageState) => state.isLoading
  );
}
