import { createAction } from '@ngrx/store';

export class PublicForgotPasswordPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PublicForgotPasswordPage] Reset State'
  );
  /* tslint:enable:typedef */
}
