export class PublicForgotPasswordPageState {
  public isLoading: boolean;

  constructor() {
    this.isLoading = false;
  }
}
