import { Action, createReducer, on } from '@ngrx/store';
import { PublicForgotPasswordPageActions } from './actions';
import { PublicForgotPasswordPageState } from './state';

const initialState = new PublicForgotPasswordPageState();

const reducer = createReducer(
  initialState,
  on(PublicForgotPasswordPageActions.resetState, () => initialState)
);

export function publicForgotPasswordPageReducer(state: PublicForgotPasswordPageState | undefined, action: Action): PublicForgotPasswordPageState {
  return reducer(state, action);
}
