import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'public-forgot-password-page',
  templateUrl: 'forgot-password.html',
  styleUrls: ['forgot-password.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicForgotPasswordPageComponent {

}
