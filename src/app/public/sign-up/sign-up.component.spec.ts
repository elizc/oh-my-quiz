import { PublicSignUpPageComponent } from './sign-up.component';
import { PublicSignUpPageEffects, publicSignUpPageReducer } from './shared/store';
import { AppState } from '@shared/store/state';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { render, RenderResult } from '@testing-library/angular';
import { configuration } from '@configuration';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('PublicSignUpPageComponent', () => {
  let component: RenderResult<PublicSignUpPageComponent>;
  let componentInstance: PublicSignUpPageComponent;
  let store: Store<AppState>;

  const translation = require(`../../../assets/i18n/${configuration.languages.default}.json`);

  beforeEach(async () => {
    component = await render(PublicSignUpPageComponent, {
      imports: [
        TranslateTestingModule.withTranslations(configuration.languages.default, translation),
        StoreModule.forRoot({}, {
          runtimeChecks: {
            strictStateImmutability: true,
            strictActionImmutability: true
          }
        }),
        StoreModule.forFeature('publicSignUpPage', publicSignUpPageReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([PublicSignUpPageEffects])
      ],
      declarations: [
        PublicSignUpPageComponent
      ],
      routes: [],
      providers: []
    });

    componentInstance = component.fixture.componentInstance;
    store = TestBed.inject(Store);
  });

  it('should create', async () => {
    expect(component).toBeDefined();
  });
});

