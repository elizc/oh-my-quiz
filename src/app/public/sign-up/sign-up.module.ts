import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PublicSignUpPageComponent } from './sign-up.component';
import { PublicSignUpPageRoutingModule } from './sign-up.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { publicSignUpPageReducer, PublicSignUpPageEffects } from './shared/store';
import { NgrxFormsModule } from 'ngrx-forms';

@NgModule({
  declarations: [
    PublicSignUpPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    PublicSignUpPageRoutingModule,
    NgrxFormsModule,
    StoreModule.forFeature('publicSignUpPage', publicSignUpPageReducer),
    EffectsModule.forFeature([PublicSignUpPageEffects])
  ],
  providers: []
})
export class PublicSignUpPageModule { }
