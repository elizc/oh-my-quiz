import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PublicSignUpPageState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { PublicSignUpPageForm } from '@app/public/sign-up/shared/forms';

const selectFeature = (state: AppState) => state.publicSignUpPage;

export class PublicSignUpPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PublicSignUpPageState) => state.isLoading
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<PublicSignUpPageForm>> = createSelector(
    selectFeature,
    (state: PublicSignUpPageState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: PublicSignUpPageState) => state.errorResponse
  );
}
