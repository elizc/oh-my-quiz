import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

export class PublicSignUpPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PublicSignUpPage] Reset State'
  );

  public static submitForm = createAction(
    '[PublicSignUpPage] Submit Form'
  );

  public static signUp = createAction(
    '[PublicSignUpPage] Sign Up'
  );

  public static signUpSuccess = createAction(
    '[PublicSignUpPage] Sign Up Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static signUpFailure = createAction(
    '[PublicSignUpPage] Sign Up Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
