import { Action, createReducer, on } from '@ngrx/store';
import { PublicSignUpPageActions } from './actions';
import { PublicSignUpPageState } from './state';
import { authActions } from '@shared/auth';
import { onNgrxForms, updateGroup, validate, wrapReducerWithFormStateUpdate } from 'ngrx-forms';
import { email, required } from 'ngrx-forms/validation';
import { PublicSignUpPageForm } from '@app/public/sign-up/shared/forms';

const initialState = new PublicSignUpPageState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(PublicSignUpPageActions.resetState, () => initialState),
  on(authActions.register, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(authActions.registerSuccess, (state) => ({
    ...state,
    isLoading: false,
    errorResponse: null
  })),
  on(authActions.registerFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  updateGroup<PublicSignUpPageForm>({
    name: validate(required),
    email: validate(required, email),
    password: validate(required)
  })
);

export function publicSignUpPageReducer(state: PublicSignUpPageState | undefined, action: Action): PublicSignUpPageState {
  return reducer(state, action);
}
