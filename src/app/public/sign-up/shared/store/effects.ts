import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, tap, withLatestFrom } from 'rxjs/operators';
import { authActions } from '@shared/auth';
import { PublicSignUpPageActions } from '@app/public/sign-up/shared/store/actions';
import { PublicSignUpPageSelectors } from '@app/public/sign-up/shared/store/selectors';
import { User } from '@shared/user';

@Injectable()
export class PublicSignUpPageEffects {
  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicSignUpPageActions.submitForm),
      withLatestFrom(this.store.select(PublicSignUpPageSelectors.formState)),
      filter(([_, formState]) => formState.isValid),
      map(([_, formState]) => {
        const user = new User({
          name: formState.value.name,
          email: formState.value.email,
          password: formState.value.password,
          confirm: formState.value.password
        });

        return authActions.register({ user });
      })
    )
  );

  public signupSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.registerSuccess),
      tap(() => this.router.navigate(['/account']))
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<AppState>
  ) { }
}
