import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { PublicSignUpPageForm } from '@app/public/sign-up/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';

export class PublicSignUpPageState {
  public isLoading: boolean;
  public formState: FormGroupState<PublicSignUpPageForm>;
  public errorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.formState = createFormGroupState<PublicSignUpPageForm>('PublicSignUpPageForm', {
      name: '',
      email: '',
      password: ''
    });
    this.errorResponse = null;
  }
}
