import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PublicSignUpPageForm } from '@app/public/sign-up/shared/forms';
import { PublicSignUpPageActions, PublicSignUpPageSelectors } from '@app/public/sign-up/shared/store';

@Component({
  selector: 'public-sign-up-page',
  templateUrl: 'sign-up.html',
  styleUrls: ['sign-up.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicSignUpPageComponent implements OnDestroy {
  public formState$: Observable<FormGroupState<PublicSignUpPageForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;

  constructor(private store: Store<AppState>) {
    this.formState$ = this.store.select(PublicSignUpPageSelectors.formState);
    this.isLoading$ = this.store.select(PublicSignUpPageSelectors.isLoading);
    this.errorResponse$ = this.store.select(PublicSignUpPageSelectors.errorResponse);
  }

  public ngOnDestroy(): void {
    this.store.dispatch(PublicSignUpPageActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(PublicSignUpPageActions.submitForm());
  }
}
