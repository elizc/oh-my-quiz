import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'public-reset-password-page',
  templateUrl: 'reset-password.html',
  styleUrls: ['reset-password.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicResetPasswordPageComponent {

}
