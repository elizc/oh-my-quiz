import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PublicResetPasswordPageState } from './state';

const selectFeature = (state: AppState) => state.publicResetPasswordPage;

export class PublicResetPasswordPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PublicResetPasswordPageState) => state.isLoading
  );
}
