export class PublicResetPasswordPageState {
  public isLoading: boolean;

  constructor() {
    this.isLoading = false;
  }
}
