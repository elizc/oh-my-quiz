import { createAction } from '@ngrx/store';

export class PublicResetPasswordPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PublicResetPasswordPage] Reset State'
  );
  /* tslint:enable:typedef */
}
