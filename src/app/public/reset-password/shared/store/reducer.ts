import { Action, createReducer, on } from '@ngrx/store';
import { PublicResetPasswordPageActions } from './actions';
import { PublicResetPasswordPageState } from './state';

const initialState = new PublicResetPasswordPageState();

const reducer = createReducer(
  initialState,
  on(PublicResetPasswordPageActions.resetState, () => initialState)
);

export function publicResetPasswordPageReducer(state: PublicResetPasswordPageState | undefined, action: Action): PublicResetPasswordPageState {
  return reducer(state, action);
}
