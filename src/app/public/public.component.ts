import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { NavigationSelectors } from '@shared/navigation';

@Component({
  selector: 'app-public-root',
  templateUrl: 'public.html',
  styleUrls: ['public.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicComponent {
  public url$: Observable<string>;

  constructor(private store: Store<AppState>) {
    this.url$ = this.store.select(NavigationSelectors.url);
  }
}
