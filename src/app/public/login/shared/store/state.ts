import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { PublicLoginPageForm } from '@app/public/login/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';

export class PublicLoginPageState {
  public isLoading: boolean;
  public formState: FormGroupState<PublicLoginPageForm>;
  public errorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.formState = createFormGroupState<PublicLoginPageForm>('PublicLoginPageForm', {
      email: '',
      password: ''
    });
    this.errorResponse = null;
  }
}
