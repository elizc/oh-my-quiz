import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

export class PublicLoginPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PublicLoginPage] Reset State'
  );

  public static submitForm = createAction(
    '[PublicLoginPage] Submit Form'
  );

  public static login = createAction(
    '[PublicLoginPage] Login'
  );

  public static loginSuccess = createAction(
    '[PublicLoginPage] Login Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static loginFailure = createAction(
    '[PublicLoginPage] Login Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
