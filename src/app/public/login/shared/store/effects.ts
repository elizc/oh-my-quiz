import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PublicLoginPageActions, PublicLoginPageSelectors  } from '@app/public/login/shared/store';
import { filter, map, tap, withLatestFrom } from 'rxjs/operators';
import { authActions, AuthCredentials } from '@shared/auth';
import { Router } from '@angular/router';

@Injectable()
export class PublicLoginPageEffects {
  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicLoginPageActions.submitForm),
      withLatestFrom(this.store.select(PublicLoginPageSelectors.formState)),
      filter(([_, formState]) => formState.isValid),
      map(([_, formState]) => {
        const credentials = new AuthCredentials({
          email: formState.value.email,
          password: formState.value.password
        });

        return authActions.authorize({ credentials });
      })
    )
  );

  public loginSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.authorizeSuccess),
      tap(() => this.router.navigate(['/account']))
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<AppState>
  ) { }
}
