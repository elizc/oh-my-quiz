import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PublicLoginPageState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { PublicLoginPageForm } from '@app/public/login/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';

const selectFeature = (state: AppState) => state.publicLoginPage;

export class PublicLoginPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PublicLoginPageState) => state.isLoading
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<PublicLoginPageForm>> = createSelector(
    selectFeature,
    (state: PublicLoginPageState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: PublicLoginPageState) => state.errorResponse
  );
}
