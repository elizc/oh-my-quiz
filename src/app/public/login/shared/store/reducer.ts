import { Action, createReducer, on } from '@ngrx/store';
import { PublicLoginPageActions } from './actions';
import { PublicLoginPageState } from './state';
import { onNgrxForms, updateGroup, validate, wrapReducerWithFormStateUpdate } from 'ngrx-forms';
import { PublicLoginPageForm } from '@app/public/login/shared/forms';
import { email, required } from 'ngrx-forms/validation';
import { authActions } from '@shared/auth';

const initialState = new PublicLoginPageState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(PublicLoginPageActions.resetState, () => initialState),
  on(authActions.authorize, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(authActions.authorizeSuccess, (state) => ({
    ...state,
    isLoading: false,
    errorResponse: null
  })),
  on(authActions.authorizeFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  updateGroup<PublicLoginPageForm>({
    email: validate(required, email),
    password: validate(required)
  })
);

export function publicLoginPageReducer(state: PublicLoginPageState | undefined, action: Action): PublicLoginPageState {
  return reducer(state, action);
}
