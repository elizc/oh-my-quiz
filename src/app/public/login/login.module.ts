import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PublicLoginPageComponent } from './login.component';
import { PublicLoginPageRoutingModule } from './login.routing';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { publicLoginPageReducer, PublicLoginPageEffects } from './shared/store';
import { NgrxFormsModule } from 'ngrx-forms';
import { WebpackTranslateLoader } from '@app/app.translate.loader';

@NgModule({
  declarations: [
    PublicLoginPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useClass: WebpackTranslateLoader
      }
    }),
    PublicLoginPageRoutingModule,
    NgrxFormsModule,
    StoreModule.forFeature('publicLoginPage', publicLoginPageReducer),
    EffectsModule.forFeature([PublicLoginPageEffects])
  ],
  providers: []
})
export class PublicLoginPageModule { }
