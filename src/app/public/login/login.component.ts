import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { PublicLoginPageForm } from '@app/public/login/shared/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PublicLoginPageActions, PublicLoginPageSelectors } from './shared/store';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'public-login-page',
  templateUrl: 'login.html',
  styleUrls: ['login.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicLoginPageComponent implements OnDestroy {
  public formState$: Observable<FormGroupState<PublicLoginPageForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;

  constructor(private store: Store<AppState>) {
    this.formState$ = this.store.select(PublicLoginPageSelectors.formState);
    this.isLoading$ = this.store.select(PublicLoginPageSelectors.isLoading);
    this.errorResponse$ = this.store.select(PublicLoginPageSelectors.errorResponse);
  }

  public ngOnDestroy(): void {
    this.store.dispatch(PublicLoginPageActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(PublicLoginPageActions.submitForm());
  }
}
