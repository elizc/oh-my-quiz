import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { PublicHomePageForm } from '@app/public/home/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';

export class PublicHomePageState {
  public isLoading: boolean;
  public formState: FormGroupState<PublicHomePageForm>;
  public errorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.formState = createFormGroupState<PublicHomePageForm>('PublicHomePageForm', {
      pin: null
    });
    this.errorResponse = null;
  }
}
