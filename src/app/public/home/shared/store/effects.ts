import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap, withLatestFrom, mergeMap } from 'rxjs/operators';
import { PublicHomePageActions } from '@app/public/home/shared/store/actions';
import { PublicHomePageSelectors } from '@app/public/home/shared/store/selectors';
import { GameService } from '@shared/game';
import { Router } from '@angular/router';
import { PlayerActions } from '@shared/player';
import { NavigationSelectors } from '@shared/navigation';

@Injectable()
export class PublicHomePageEffects {
  public initPage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicHomePageActions.initPage),
      withLatestFrom(this.store.select(NavigationSelectors.queryParam, 'code')),
      filter(([_, code]) => !!code),
      mergeMap(([_, code]) => [
        PublicHomePageActions.prefillFormState({ code: Number(code) }),
        PublicHomePageActions.searchGame({ code: Number(code) })
      ])
    )
  );

  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicHomePageActions.submitForm),
      withLatestFrom(this.store.select(PublicHomePageSelectors.formState)),
      filter(([_, formState]) => formState.isValid),
      map(([_, formState]) => PublicHomePageActions.searchGame({ code: formState.value.pin }))
    )
  );

  public searchGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicHomePageActions.searchGame),
      switchMap((action) => {
        return this.gameService.loadByCode(action.code, { with: ['quiz.img'] })
          .pipe(
            map((game) => PublicHomePageActions.searchGameSuccess({ game })),
            catchError((response) => of(PublicHomePageActions.searchGameFailure({ response })))
          );
      })
    )
  );

  public searchGameSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PublicHomePageActions.searchGameSuccess),
      map((action) => PlayerActions.setGame({ game: action.game })),
      tap(() => this.router.navigate(['/player/join']))
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private gameService: GameService
  ) { }
}
