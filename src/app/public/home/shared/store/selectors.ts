import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PublicHomePageState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { PublicHomePageForm } from '@app/public/home/shared/forms';

const selectFeature = (state: AppState) => state.publicHomePage;

export class PublicHomePageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PublicHomePageState) => state.isLoading
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<PublicHomePageForm>> = createSelector(
    selectFeature,
    (state: PublicHomePageState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: PublicHomePageState) => state.errorResponse
  );
}
