import { createAction, props } from '@ngrx/store';
import { Game } from '@shared/game';
import { HttpErrorResponse } from '@angular/common/http';

export class PublicHomePageActions {
  /* tslint:disable:typedef */
  public static initPage = createAction(
    '[PublicHomePage] Init Page'
  );

  public static prefillFormState = createAction(
    '[PublicHomePage] Prefill Form State',
    props<{ code: number }>()
  );

  public static resetState = createAction(
    '[PublicHomePage] Reset State'
  );

  public static submitForm = createAction(
    '[PublicHomePage] Submit Form'
  );

  public static searchGame = createAction(
    '[PublicHomePage] Search Game',
    props<{ code: number }>()
  );

  public static searchGameSuccess = createAction(
    '[PublicHomePage] Search Game Success',
    props<{ game: Game }>()
  );

  public static searchGameFailure = createAction(
    '[PublicHomePage] Search Game Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
