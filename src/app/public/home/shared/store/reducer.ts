import { Action, createReducer, on } from '@ngrx/store';
import { PublicHomePageActions } from './actions';
import { PublicHomePageState } from './state';
import { onNgrxForms, updateGroup, validate, wrapReducerWithFormStateUpdate, setValue } from 'ngrx-forms';
import { greaterThan, required } from 'ngrx-forms/validation';
import { PublicHomePageForm } from '@app/public/home/shared/forms';

const initialState = new PublicHomePageState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(PublicHomePageActions.resetState, () => initialState),
  on(PublicHomePageActions.prefillFormState, (state, action) => ({
    ...state,
    formState: updateGroup<PublicHomePageForm>(
      state.formState,
      {
        pin: setValue(action.code)
      }
    )
  })),
  on(PublicHomePageActions.searchGame, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(PublicHomePageActions.searchGameSuccess, (state, action) => ({
    ...state,
    isLoading: false
  })),
  on(PublicHomePageActions.searchGameFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  updateGroup<PublicHomePageForm>({
    pin: validate([
      required,
      greaterThan(0)
    ])
  })
);

export function publicHomePageReducer(state: PublicHomePageState | undefined, action: Action): PublicHomePageState {
  return reducer(state, action);
}
