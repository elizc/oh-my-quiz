import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PublicHomePageComponent } from './home.component';
import { PublicHomePageRoutingModule } from './home.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { publicHomePageReducer, PublicHomePageEffects } from './shared/store';
import { NgrxFormsModule } from 'ngrx-forms';

@NgModule({
  declarations: [
    PublicHomePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgrxFormsModule,
    PublicHomePageRoutingModule,
    StoreModule.forFeature('publicHomePage', publicHomePageReducer),
    EffectsModule.forFeature([PublicHomePageEffects])
  ],
  providers: []
})
export class PublicHomePageModule { }
