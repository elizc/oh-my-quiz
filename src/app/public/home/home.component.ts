import { Component, ChangeDetectionStrategy, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { PublicHomePageForm } from '@app/public/home/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PublicHomePageActions, PublicHomePageSelectors } from '@app/public/home/shared/store';

@Component({
  selector: 'public-home-page',
  templateUrl: 'home.html',
  styleUrls: ['home.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicHomePageComponent implements OnInit, OnDestroy {
  public formState$: Observable<FormGroupState<PublicHomePageForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;

  constructor(private store: Store<AppState>) {
    this.formState$ = this.store.select(PublicHomePageSelectors.formState);
    this.isLoading$ = this.store.select(PublicHomePageSelectors.isLoading);
    this.errorResponse$ = this.store.select(PublicHomePageSelectors.errorResponse);
  }

  public ngOnInit(): void {
    this.store.dispatch(PublicHomePageActions.initPage());
  }

  public ngOnDestroy(): void {
    this.store.dispatch(PublicHomePageActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(PublicHomePageActions.submitForm());
  }
}
