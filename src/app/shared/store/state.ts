import { RouterReducerState } from '@ngrx/router-store';
import { AccountQuizesPageState } from '@app/account/quizes/shared/store';
import { AccountQuizesViewPageState } from '@app/account/quizes/view/shared/store';
import { PlayerJoinPageState } from '@app/player/join/shared/store';
import { PublicSignUpPageState } from '@app/public/sign-up/shared/store';
import { PublicHomePageState } from '@app/public/home/shared/store';
import { PublicLoginPageState } from '@app/public/login/shared/store';
import { AuthState } from '@shared/auth';
import { UserState } from '@shared/user';
import { PublicForgotPasswordPageState } from '@app/public/forgot-password/shared/store';
import { PublicResetPasswordPageState } from '@app/public/reset-password/shared/store';
import { AccountQuizesEditModalState } from '@app/account/quizes/shared/modals/edit-modal/store';
import { PresenterState } from '@shared/presenter';
import { PlayerState } from '@shared/player';
import { PlayerGamePageState } from '@app/player/game/shared/store';
import { PresenterGamePageState } from '@app/presenter/game/shared/store';
import { AccountQuizesEditQuestionModalState } from '@app/account/quizes/shared/modals/edit-question-modal/store';

export class AppState {
  public router: RouterReducerState<any>;
  public authState: AuthState;
  public userState: UserState;
  public accountQuizesPage?: AccountQuizesPageState;
  public accountQuizesEditModal?: AccountQuizesEditModalState;
  public accountQuizesViewPage?: AccountQuizesViewPageState;
  public player: PlayerState;
  public accountQuizesEditQuestionModal?: AccountQuizesEditQuestionModalState;
  public playerJoinPage?: PlayerJoinPageState;
  public playerGamePage?: PlayerGamePageState;
  public presenter?: PresenterState;
  public publicForgotPasswordPage?: PublicForgotPasswordPageState;
  public publicHomePage?: PublicHomePageState;
  public publicLoginPage?: PublicLoginPageState;
  public publicResetPasswordPage?: PublicResetPasswordPageState;
  public publicSignUpPage?: PublicSignUpPageState;
  public presenterGamePage?: PresenterGamePageState;
}
