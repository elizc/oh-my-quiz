import { NgModule } from '@angular/core';
import { QuizService } from './quiz.service';

@NgModule({
  providers: [
    QuizService
  ]
})
export class QuizModule { }
