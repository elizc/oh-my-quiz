import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { QuizFilters, Quiz } from '@shared/quiz/models';
import { PaginationRequest, PaginationResponse } from '@shared/pagination';
import { Observable } from 'rxjs';
import { classToPlain, plainToClass, plainToClassFromExist } from 'class-transformer';
import { map } from 'rxjs/operators';

@Injectable()
export class QuizService {
  constructor(
    private apiService: ApiService
  ) {
  }

  public search({ filters, ...params }: {
    page?: number,
    perPage?: number,
    filters?: QuizFilters,
    orderBy?: string,
    desc?: boolean,
    all?: boolean,
    with?: Array<string>,
    withCount?: Array<string>
  } = {}): Observable<PaginationResponse<Quiz>> {
    const request = new PaginationRequest({ ...filters, ...params });

    return this.apiService
      .get<PaginationResponse<Quiz>>('/quizzes', classToPlain<PaginationRequest>(request))
      .pipe(
        map((response) => plainToClassFromExist(new PaginationResponse<Quiz>(Quiz), response, { groups: ['main'] }))
      );
  }

  public create(quiz: Quiz): Observable<Quiz> {
    return this.apiService
      .post('/quizzes', classToPlain<Quiz>(quiz, { groups: ['create'] }))
      .pipe(
        map((response) => plainToClass(Quiz, response, { groups: ['main'] }))
      );
  }

  public update(quiz: Quiz): Observable<Quiz> {
    return this.apiService
      .put(`/quizzes/${quiz.id}`, classToPlain<Quiz>(quiz, { groups: ['update'] }))
      .pipe(
        map(() => quiz)
      );
  }

  public loadByID(id: number, relation: Array<string>): Observable<Quiz> {
    return this.apiService
      .get(`/quizzes/${id}`, { with: relation })
      .pipe(
        map((response) => plainToClass(Quiz, response, { groups: ['main'] }))
      );
  }
}
