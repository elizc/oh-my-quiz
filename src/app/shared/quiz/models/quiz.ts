import { Expose, Type } from 'class-transformer';
import { Media } from '@shared/media';

export class Quiz {
  @Expose({ groups: ['main', 'update'] })
  public id: number;

  @Expose({ groups: ['main', 'create', 'update'] })
  public name: string;

  @Expose({ groups: ['main', 'create', 'update'] })
  public capacity: number;

  @Type(() => Media)
  @Expose({ name: 'img', groups: ['main'] })
  public image: Media;

  @Expose({ name: 'img_id', groups: ['main', 'create', 'update'] })
  public imageID: number;

  @Expose({ name: 'is_time_used_for_scoring', groups: ['main', 'create', 'update'] })
  public isConsiderResponseSpeed: boolean;

  @Expose({ name: 'is_show_statistic', groups: ['main', 'create', 'update'] })
  public isShowStatistics: boolean;

  @Expose({ name: 'is_show_right_answer', groups: ['main', 'create', 'update'] })
  public isShowAnswer: boolean;

  @Expose({ name: 'questions_count', groups: ['main'] })
  public questionsCount: number;

  constructor(quiz: Partial<Quiz>) {
    Object.assign(this, quiz);
  }
}
