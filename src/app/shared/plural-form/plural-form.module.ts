import { I18nPluralPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { PluralFormPipe } from './plural-form.pipe';

@NgModule({
  declarations: [
    PluralFormPipe
  ],
  providers: [
    I18nPluralPipe
  ],
  exports: [
    PluralFormPipe
  ]
})
export class PluralFormModule { }
