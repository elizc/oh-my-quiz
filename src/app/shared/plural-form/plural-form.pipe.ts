import { I18nPluralPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'pluralForm'
})
export class PluralFormPipe implements PipeTransform {
  constructor(
    private i18nPluralPipe: I18nPluralPipe,
    private translateService: TranslateService
  ) { }

  public async transform(value: any, translationKey: string): Promise<string> {
    const translation = await this.translateService.get(translationKey).toPromise();

    return this.i18nPluralPipe.transform(value, {
      one: translation['TEXT_ONE'],
      few: translation['TEXT_FEW'],
      many: translation['TEXT_MANY'],
      other: translation['TEXT_OTHER']
    });
  }
}
