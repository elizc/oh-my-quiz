import { NgModule } from '@angular/core';
import { AnswerService } from './answer.service';

@NgModule({
  providers: [
    AnswerService
  ]
})
export class AnswerModule { }
