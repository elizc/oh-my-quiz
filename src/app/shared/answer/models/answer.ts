import { Expose } from 'class-transformer';

export class Answer {
  @Expose({ groups: ['main'] })
  public id: number;

  @Expose({ name: 'game_question_id', groups: ['main', 'send'] })
  public gameQuestionId: number;

  @Expose({ name: 'answer', groups: ['main', 'send'] })
  public name: string;

  @Expose({ groups: ['main', 'send'] })
  public token: string;

  @Expose({ groups: ['main'] })
  public time: number;

  @Expose({ name: 'team_id', groups: ['main'] })
  public teamId: number;

  @Expose({ groups: ['main'] })
  public score: number;

  constructor(answer: Partial<Answer>) {
    Object.assign(this, answer);
  }
}
