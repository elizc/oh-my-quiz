import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { Observable } from 'rxjs';
import { Answer } from '@shared/answer/models';
import { classToPlain, plainToClass } from 'class-transformer';
import { map } from 'rxjs/operators';

@Injectable()
export class AnswerService {
  constructor(
    private apiService: ApiService
  ) {
  }

  public sendAnswer(answer: Answer): Observable<Answer> {
    return this.apiService
      .post('/game-question-answers', classToPlain<Answer>(answer, { groups: ['send'] }))
      .pipe(
        map((response) => plainToClass(Answer, response, { groups: ['main'] }))
      );
  }
}
