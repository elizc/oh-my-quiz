import * as io from 'socket.io-client';
import Echo from 'laravel-echo';
import { configuration } from '@configuration';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';

@Injectable()
export class EchoService {
  public get server(): Echo {
    return this._server;
  }

  public _server: Echo;

  constructor(private store: Store<AppState>) {}

  public init(): void {
    this.destroy();

    this._server = new Echo({
      broadcaster: 'socket.io',
      host: configuration.echo.url,
      client: io
    });
  }

  public destroy(): void {
    if (this.server) {
      this._server.disconnect();
      this._server = null;
    }
  }
}
