import { EchoService } from './echo.service';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [
    EchoService
  ]
})
export class EchoModule { }
