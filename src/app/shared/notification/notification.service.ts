import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { configuration } from '@configuration';

@Injectable()
export class NotificationService {
  constructor(
    private toastr: ToastrService
  ) { }

  public error(message: string): void {
    this.toastr.error(message, null, configuration.notification);
  }

  public success(message: string): void {
    this.toastr.success(message, null, configuration.notification);
  }
}
