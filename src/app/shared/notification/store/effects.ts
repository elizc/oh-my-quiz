import { ofType, createEffect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { notificationActions } from './actions';
import { tap, mergeMap } from 'rxjs/operators';
import { NotificationService } from '../notification.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class NotificationEffects {
  public showError$: Observable<void> = createEffect(
    () => this.actions$.pipe(
      ofType(notificationActions.showError),
      mergeMap((action) => this.translateService.get(action.translationKey)),
      tap((translation) => this.notificationService.error(translation))
    ),
    { dispatch: false }
  );

  public showSuccess$: Observable<void> = createEffect(
    () => this.actions$.pipe(
      ofType(notificationActions.showSuccess),
      mergeMap((action) => this.translateService.get(action.translationKey)),
      tap((translation) => this.notificationService.success(translation))
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private notificationService: NotificationService,
    private translateService: TranslateService
  ) { }
}
