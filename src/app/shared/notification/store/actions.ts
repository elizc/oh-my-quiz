import { createAction, props } from '@ngrx/store';

export const notificationActions = {
  showError: createAction(
    '[Notification] Show error',
    props<{ translationKey: string }>()
  ),
  showSuccess: createAction(
    '[Notification] Show success',
    props<{ translationKey: string }>()
  )
};
