import { NgModule } from '@angular/core';
import { HasPlayerGameGuard } from '@shared/player/guards';
import { HasPlayerTeamGuard } from '@shared/player/guards/has-player-team-guard.service';

@NgModule({
  providers: [
    HasPlayerGameGuard,
    HasPlayerTeamGuard
  ]
})
export class PlayerModule { }
