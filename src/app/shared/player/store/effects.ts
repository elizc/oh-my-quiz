import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { interval, Observable, of, race } from 'rxjs';
import { catchError, delay, exhaustMap, filter, map, skip, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Game, GameService } from '@shared/game';
import { EchoService } from '@shared/echo';
import { PlayerActions, PlayerSelectors } from '@shared/player';
import { Router } from '@angular/router';
import { Team, TeamService } from '@shared/team';
import { classToPlain, plainToClass } from 'class-transformer';
import { Question } from '@shared/question';
import { Answer, AnswerService } from '@shared/answer';

@Injectable()
export class PlayerEffects {
  public init$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.init),
      withLatestFrom(
        this.store.select(PlayerSelectors.game),
        this.store.select(PlayerSelectors.team)
      ),
      filter(([_, game, team]) => !!game && !!team),
      map(() => PlayerActions.tryConnect())
    )
  );

  public setGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.setGame),
      tap((action) => localStorage.setItem('game', JSON.stringify(classToPlain<Game>(action.game, { groups: ['main'] }))))
    ),
    { dispatch: false }
  );

  public join$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.join),
      tap((action) => localStorage.setItem('team', JSON.stringify(classToPlain<Team>(action.team, { groups: ['main'] }))))
    ),
    { dispatch: false }
  );

  public tryConnect$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.tryConnect),
      tap(() => {
        this.echoService.init();

        this.echoService.server.connector.socket.on('connect', () => this.store.dispatch(PlayerActions.connected()));
        this.echoService.server.connector.socket.on('disconnect', () => this.store.dispatch(PlayerActions.disconnected()));
      })
    ),
    { dispatch: false }
  );

  public connect$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.connected),
      withLatestFrom(this.store.select(PlayerSelectors.game)),
      tap(([_, game]) => {
        const channel = this.echoService.server.channel(`Game ${ game.id }`);

        channel.listen('TeamJoin', (event) => {
          const teams = event.allTeams.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          this.store.dispatch(PlayerActions.teamJoined({ teams }));
        });

        channel.listen('GameStart', (event) => {
          this.store.dispatch(PlayerActions.gameStarted({ questionsCount: event.game.game_questions_count }));
        });

        channel.listen('NextQuestion', (event) => {
          const question = plainToClass(Question, event.question.question, { groups: ['main'] });
          const gameQuestionId = event.question.id;

          this.store.dispatch(PlayerActions.nextQuestionReceived({ question, gameQuestionId }));
        });

        channel.listen('ResultsData', (event) => {
          const teams = event.teamScore.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          this.store.dispatch(PlayerActions.questionResultsReceived({ teams }));
        });

        channel.listen('GameFinish', (event) => {
          const teams = event.teams.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          this.store.dispatch(PlayerActions.gameFinished({ teams }));
        });
      }),
      map(() => PlayerActions.loadTeams())
    )
  );

  public loadTeams$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.loadTeams),
      withLatestFrom(this.store.select(PlayerSelectors.game)),
      exhaustMap(([_, game]) => {
        return this.gameService.loadByCode(game.code, { with: ['teams'] })
          .pipe(
            map((game) => PlayerActions.loadTeamsSuccess({ teams: game.teams })),
            catchError((response) => of(PlayerActions.loadTeamsFailure({ response })))
          );
      })
    )
  );

  public gameStarted$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.gameStarted),
      map(() => PlayerActions.startGetReadyTimer())
    )
  );

  public startGetReadyTimer$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.startGetReadyTimer),
      tap(() => this.store.dispatch(PlayerActions.setGetReadyTimer({ seconds: 3 }))),
      delay(1000),
      tap(() => this.store.dispatch(PlayerActions.setGetReadyTimer({ seconds: 2 }))),
      delay(1000),
      tap(() => this.store.dispatch(PlayerActions.setGetReadyTimer({ seconds: 1 }))),
      delay(1000),
      tap(() => this.store.dispatch(PlayerActions.setGetReadyTimer({ seconds: null })))
    ),
    { dispatch: false }
  );

  public nextQuestionReceived$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.nextQuestionReceived),
      map(() => PlayerActions.startQuestionTimer())
    )
  );

  public startQuestionTimer$: Observable<Action | number> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.startQuestionTimer),
      withLatestFrom(this.store.select(PlayerSelectors.question)),
      switchMap(([_, question]) => race(
        interval(1000).pipe(
          take(question.time + 1),
          tap((count) => {
            this.store.dispatch(PlayerActions.setQuestionTimer({ seconds: question.time - count }));
          }),
          skip(question.time)
        ),
        this.actions$.pipe(ofType(PlayerActions.questionResultsReceived))
      ))
    ),
    { dispatch: false }
  );

  public sendAnswer$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.sendAnswer),
      withLatestFrom(
        this.store.select(PlayerSelectors.gameQuestionId),
        this.store.select(PlayerSelectors.team)
      ),
      exhaustMap(([action, gameQuestionId, team]) => {
        const answer = new Answer({
          gameQuestionId,
          name: action.selectedAnswerName,
          token: team.token
        });

        return this.answerService.sendAnswer(answer)
          .pipe(
            map((answer) => PlayerActions.sendAnswerSuccess({ answer })),
            catchError((response) => of(PlayerActions.sendAnswerFailure({ response })))
          );
      })
    )
  );

  public questionResultsReceived$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.questionResultsReceived),
      delay(5000),
      map(() => PlayerActions.showQuestionResults())
    )
  );

  public quit$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerActions.quit),
      map(() => PlayerActions.resetState()),
      tap(() => this.router.navigate(['/']))
    )
  );

  constructor(
    private actions$: Actions,
    private gameService: GameService,
    private echoService: EchoService,
    private teamService: TeamService,
    private answerService: AnswerService,
    private router: Router,
    private store: Store<AppState>
  ) {
  }
}
