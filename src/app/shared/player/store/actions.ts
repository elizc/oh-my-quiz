import { createAction, props } from '@ngrx/store';
import { Game } from '@shared/game';
import { HttpErrorResponse } from '@angular/common/http';
import { Team } from '@shared/team';
import { Question } from '@shared/question';
import { Answer } from '@shared/answer';

export class PlayerActions {
  /* tslint:disable:typedef */
  public static init = createAction(
    '[PlayerActions] Init'
  );

  public static resetState = createAction(
    '[PlayerActions] Reset State'
  );

  public static setGame = createAction(
    '[PlayerActions] Set Game',
    props<{ game: Game }>()
  );

  public static join = createAction(
    '[PlayerActions] Join',
    props<{ team: Team }>()
  );

  public static tryConnect = createAction(
    '[PlayerActions] Try Connect'
  );

  public static connected = createAction(
    '[PlayerActions] Connected'
  );

  public static disconnected = createAction(
    '[PlayerActions] Disconnected'
  );

  public static loadTeams = createAction(
    '[PlayerActions] Load Teams'
  );

  public static loadTeamsSuccess = createAction(
    '[PlayerActions] Load Teams Success',
    props<{ teams: Array<Team> }>()
  );

  public static loadTeamsFailure = createAction(
    '[PlayerActions] Load Teams Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static teamJoined = createAction(
    '[PlayerActions] Team Joined',
    props<{ teams: Array<Team> }>()
  );

  public static gameStarted = createAction(
    '[PlayerActions] Game Started',
    props<{ questionsCount: number }>()
  );

  public static startGetReadyTimer = createAction(
    '[PlayerActions] Start Get Ready Timer'
  );

  public static setGetReadyTimer = createAction(
    '[PlayerActions] Set Get Ready Timer',
    props<{ seconds: number }>()
  );

  public static nextQuestionReceived = createAction(
    '[PlayerActions] Next Question Received',
    props<{ question: Question, gameQuestionId: number }>()
  );

  public static startQuestionTimer = createAction(
    '[PlayerActions] Start Question Timer'
  );

  public static setQuestionTimer = createAction(
    '[PlayerActions] Set Question Count',
    props<{ seconds: number }>()
  );

  public static sendAnswer = createAction(
    '[PlayerActions] Send Answer',
    props<{ selectedAnswerName: string }>()
  );

  public static sendAnswerSuccess = createAction(
    '[PlayerActions] Send Answer Success',
    props<{ answer: Answer }>()
  );

  public static sendAnswerFailure = createAction(
    '[PlayerActions] Send Answer Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static questionResultsReceived = createAction(
    '[PlayerActions] Question Results Received',
    props<{ teams: Array<Team> }>()
  );

  public static showQuestionResults = createAction(
    '[PlayerActions] Show Question Results'
  );

  public static gameFinished = createAction(
    '[PlayerActions] Game Finished',
    props<{ teams: Array<Team> }>()
  );

  public static quit = createAction(
    '[PlayerActions] Quit'
  );
  /* tslint:enable:typedef */
}
