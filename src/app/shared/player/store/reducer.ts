import { Action, createReducer, on } from '@ngrx/store';
import { PlayerActions } from './actions';
import { PlayerState } from './state';
import { GameStage } from '@shared/game';

const initialState = new PlayerState();

const reducer = createReducer(
  initialState,
  on(PlayerActions.resetState, () => initialState),
  on(PlayerActions.setGame, (state, action) => ({
    ...initialState,
    game: action.game
  })),
  on(PlayerActions.join, (state, action) => ({
    ...state,
    team: action.team
  })),
  on(PlayerActions.connected, (state) => ({
    ...state,
    stage: GameStage.WAIT
  })),
  on(PlayerActions.loadTeamsSuccess, (state, action) => ({
    ...state,
    teams: action.teams
  })),
  on(PlayerActions.teamJoined, (state, action) => ({
    ...state,
    teams: action.teams
  })),
  on(PlayerActions.gameStarted, (state, action) => ({
    ...state,
    stage: GameStage.GET_READY,
    questionsCount: action.questionsCount
  })),
  on(PlayerActions.setGetReadyTimer, (state, action) => ({
    ...state,
    getReadyTimer: action.seconds
  })),
  on(PlayerActions.nextQuestionReceived, (state, action) => ({
    ...state,
    stage: GameStage.QUESTION,
    question: action.question,
    gameQuestionId: action.gameQuestionId,
    currentQuestionNumber: state.currentQuestionNumber + 1,
    selectedAnswer: null
  })),
  on(PlayerActions.setQuestionTimer, (state, action) => ({
    ...state,
    questionTimer: action.seconds
  })),
  on(PlayerActions.sendAnswer, (state, action) => ({
    ...state,
    selectedAnswer: action.selectedAnswerName
  })),
  on(PlayerActions.questionResultsReceived, (state, action) => ({
    ...state,
    teams: action.teams
  })),
  on(PlayerActions.showQuestionResults, (state) => ({
    ...state,
    stage: GameStage.QUESTION_RESULTS
  })),
  on(PlayerActions.gameFinished, (state, action) => ({
    ...state,
    stage: GameStage.FINAL_RESULTS,
    teams: action.teams
  }))
);

export function playerReducer(state: PlayerState | undefined, action: Action): PlayerState {
  return reducer(state, action);
}
