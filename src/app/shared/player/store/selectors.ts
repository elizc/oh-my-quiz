import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PlayerState } from './state';
import { Game, GameStage } from '@shared/game';
import { Question } from '@shared/question';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';

const selectFeature = (state: AppState) => state.player;

export class PlayerSelectors {
  public static question: MemoizedSelector<AppState, Question> = createSelector(
    selectFeature,
    (state: PlayerState) => state.question
  );

  public static team: MemoizedSelector<AppState, Team> = createSelector(
    selectFeature,
    (state: PlayerState) => state.team
  );

  public static game: MemoizedSelector<AppState, Game> = createSelector(
    selectFeature,
    (state: PlayerState) => state.game
  );

  public static quiz: MemoizedSelector<AppState, Quiz> = createSelector(
    PlayerSelectors.game,
    (game: Game) => game.quiz
  );

  public static stage: MemoizedSelector<AppState, GameStage> = createSelector(
    selectFeature,
    (state: PlayerState) => state.stage
  );

  public static teams: MemoizedSelector<AppState, Array<Team>> = createSelector(
    selectFeature,
    (state: PlayerState) => state.teams
  );

  public static getReadyTimer: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PlayerState) => state.getReadyTimer
  );

  public static questionTimer: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PlayerState) => state.questionTimer
  );

  public static formattedQuestionTimer: MemoizedSelector<AppState, string> = createSelector(
    PlayerSelectors.questionTimer,
    (questionTimer: number) => {
      const minutes = Math.floor(questionTimer / 60);
      const seconds = questionTimer % 60;

      return `${minutes}:${(seconds < 10) ? '0' : ''}${seconds}`;
    }
  );

  public static gameQuestionId: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PlayerState) => state.gameQuestionId
  );

  public static selectedAnswer: MemoizedSelector<AppState, string> = createSelector(
    selectFeature,
    (state: PlayerState) => state.selectedAnswer
  );

  public static questionsCount: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PlayerState) => state.questionsCount
  );

  public static currentQuestionNumber: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PlayerState) => state.currentQuestionNumber
  );
}
