import { Game, GameStage } from '@shared/game';
import { Question } from '@shared/question';
import { Team } from '@shared/team';
import { plainToClass } from 'class-transformer';

export class PlayerState {
  public isLoading: boolean;
  public game: Game;
  public question: Question;
  public selectedAnswer: string;
  public team: Team;
  public stage: GameStage;
  public teams: Array<Team>;
  public getReadyTimer: number;
  public questionTimer: number;
  public gameQuestionId: number;
  public questionsCount: number;
  public currentQuestionNumber: number;

  constructor() {
    this.isLoading = false;

    const team = localStorage.getItem('team');
    this.team = plainToClass(Team, JSON.parse(team), { groups: ['main'] });

    this.teams = [];

    const game = localStorage.getItem('game');
    this.game = plainToClass(Game, JSON.parse(game), { groups: ['main'] });

    this.question = null;
    this.selectedAnswer = null;
    this.stage = null;
    this.getReadyTimer = null;
    this.questionTimer = null;
    this.gameQuestionId = null;
    this.questionsCount = null;
    this.currentQuestionNumber = 0;
  }
}
