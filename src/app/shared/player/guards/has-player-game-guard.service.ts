import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerSelectors } from '@shared/player';

@Injectable()
export class HasPlayerGameGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  public canActivate(): Observable<boolean> {
    return this.store
      .pipe(
        select(PlayerSelectors.game),
        map((game) => {
          if (!game) {
            this.router.navigate(['/']);
          }

          return !!game;
        })
      );
  }
}
