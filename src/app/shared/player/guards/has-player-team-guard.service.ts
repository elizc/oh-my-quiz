import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { Observable } from 'rxjs';
import { PlayerSelectors } from '@shared/player';
import { map } from 'rxjs/operators';

@Injectable()
export class HasPlayerTeamGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  public canActivate(): Observable<boolean> {
    return this.store
      .pipe(
        select(PlayerSelectors.team),
        map((team) => {
          if (!team) {
            this.router.navigate(['/player/join']);
          }

          return !!team;
        })
      );
  }
}
