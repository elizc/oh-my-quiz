import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { PaginationRequest, PaginationResponse } from '@shared/pagination';
import { Observable } from 'rxjs';
import { classToPlain, plainToClass, plainToClassFromExist } from 'class-transformer';
import { map } from 'rxjs/operators';
import { Team, TeamFilters } from '@shared/team/models';
import { TeamPaginationRequest } from '@shared/team/models/team-pagination-request';

@Injectable()
export class TeamService {
  constructor(
    private apiService: ApiService
  ) {
  }

  public search({ filters, ...params }: {
    page?: number,
    perPage?: number,
    filters?: TeamFilters,
    orderBy?: string,
    desc?: boolean,
    all?: boolean,
    with?: Array<string>,
    withCount?: Array<string>
  } = {}): Observable<PaginationResponse<Team>> {
    const request = new TeamPaginationRequest({ ...filters, ...params });

    return this.apiService
      .get<PaginationResponse<Team>>('/teams', classToPlain<TeamPaginationRequest>(request))
      .pipe(
        map((response) => plainToClassFromExist(new PaginationResponse<Team>(Team), response, { groups: ['main'] }))
      );
  }

  public create(team: Team): Observable<Team> {
    return this.apiService
      .post('/teams', classToPlain<Team>(team, { groups: ['create'] }))
      .pipe(
        map((response) => plainToClass(Team, response, { groups: ['main'] }))
      );
  }
}
