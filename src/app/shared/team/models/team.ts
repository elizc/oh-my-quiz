import { Expose } from 'class-transformer';

export class Team {
  @Expose({ groups: ['main'] })
  public id: number;

  @Expose({ groups: ['main', 'create'] })
  public name: string;

  @Expose({ groups: ['main'] })
  public token: string;

  @Expose({ name: 'game_id', groups: ['main', 'create'] })
  public gameId: number;

  @Expose({ groups: ['main', 'create'] })
  public players: Array<string>;

  @Expose({ groups: ['main'] })
  public score: number;

  @Expose({  name: 'is_fastest', groups: ['main'] })
  public isFastest: boolean;

  @Expose({  name: 'is_series', groups: ['main'] })
  public isSeries: boolean;

  @Expose({ name: 'is_all_correct', groups: ['main'] })
  public isAllCorrect: boolean;

  @Expose({ groups: ['create'] })
  public code: number;

  constructor(team: Partial<Team>) {
    Object.assign(this, team);
  }
}
