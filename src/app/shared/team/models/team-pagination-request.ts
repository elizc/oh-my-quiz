import { PaginationRequest } from '@shared/pagination';
import { Expose } from 'class-transformer';

export class TeamPaginationRequest extends PaginationRequest {
  @Expose({ name: 'game_id' })
  public gameId: number;
}
