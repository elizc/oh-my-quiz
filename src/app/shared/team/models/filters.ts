export class TeamFilters {
  public gameId: number;

  constructor(filters: Partial<TeamFilters>) {
    Object.assign(this, filters);
  }
}
