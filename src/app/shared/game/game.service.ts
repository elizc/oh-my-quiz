import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { Observable } from 'rxjs';
import { classToPlain, plainToClass, plainToClassFromExist } from 'class-transformer';
import { map } from 'rxjs/operators';
import { Game, GameRequest, GameFilters } from '@shared/game/models';
import { HttpResponse } from '@angular/common/http';
import { PaginationResponse } from '@shared/pagination';
import { GamePaginationRequest } from '@shared/game/models/pagination-request';

@Injectable()
export class GameService {
  constructor(
    private apiService: ApiService
  ) {
  }

  public search({ filters, ...params }: {
    page?: number,
    perPage?: number,
    filters?: GameFilters,
    orderBy?: string,
    desc?: boolean,
    all?: boolean,
    with?: Array<string>,
    withCount?: Array<string>
  } = {}): Observable<PaginationResponse<Game>> {
    const request = new GamePaginationRequest({ ...filters, ...params });

    return this.apiService
      .get<PaginationResponse<Game>>('/games', classToPlain<GamePaginationRequest>(request))
      .pipe(
        map((response) => plainToClassFromExist(new PaginationResponse<Game>(Game), response, { groups: ['main'] }))
      );
  }

  public create(game: Game): Observable<Game> {
    return this.apiService
      .post('/games', classToPlain<Game>(game, { groups: ['create'] }))
      .pipe(
        map((response) => plainToClass(Game, response, { groups: ['main'] }))
      );
  }

  public loadByID(id: number): Observable<Game> {
    return this.apiService
      .get(`/games/${id}`)
      .pipe(
        map((response) => plainToClass(Game, response, { groups: ['main'] }))
      );
  }

  public loadByCode(code: number, params: {
    with?: Array<string>,
    withCount?: Array<string>
  } = {}): Observable<Game> {
    const request = new GameRequest({ ...params });

    return this.apiService
      .get(`/games/${code}/by-code`, classToPlain<GameRequest>(request))
      .pipe(
        map((response) => plainToClass(Game, response, { groups: ['main'] }))
      );
  }

  public start(id: number): Observable<HttpResponse<any>> {
    return this.apiService.put(`/games/${id}/start`);
  }

  public nextQuestion(id: number): Observable<HttpResponse<any>> {
    return this.apiService.put(`/games/${id}/next-question`);
  }

  public questionResults(id: number, gameQuestionId: number): Observable<HttpResponse<any>> {
    return this.apiService.get(`/games/${id}/results/${gameQuestionId}`);
  }

  public finish(id: number): Observable<HttpResponse<any>> {
    return this.apiService.put(`/games/${id}/finish`);
  }
}
