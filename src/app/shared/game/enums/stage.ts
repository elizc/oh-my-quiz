export enum GameStage {
  WAIT = 'wait',
  GET_READY = 'get_ready',
  QUESTION = 'question',
  QUESTION_RESULTS = 'question_results',
  FINAL_RESULTS = 'final_results'
}
