import { NgModule } from '@angular/core';
import { GameService } from './game.service';

@NgModule({
  providers: [
    GameService
  ]
})
export class GameModule { }
