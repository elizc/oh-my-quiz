import { Expose } from 'class-transformer';

export class GameFilters {
  @Expose({ name: 'is_completed', toPlainOnly: true })
  public isCompleted: boolean;

  constructor(model: Partial<GameFilters> = {}) {
    Object.assign(this, model);
  }
}
