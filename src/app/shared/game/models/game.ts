import { Expose, Type, Transform } from 'class-transformer';
import { Quiz } from '@shared/quiz';
import { Team } from '@shared/team';
import { dateTransformer } from '@shared/date-transformer';
import { DateTime } from 'luxon';

export class Game {
  @Expose({ groups: ['main'] })
  public id: number;

  @Expose({ groups: ['main'] })
  public code: number;

  @Type(() => Quiz)
  @Expose({ groups: ['main'] })
  public quiz: Quiz;

  @Type(() => Team)
  @Expose({ groups: ['main'] })
  public teams: Array<Team>;

  @Type(() => Team)
  @Expose({ name: 'teams_score', groups: ['main'] })
  public teamsScore: Array<Team>;

  @Expose({ name: 'quiz_id', groups: ['main', 'create'] })
  public quizId: number;

  @Expose({ name: 'created_at', groups: ['main'] })
  @Transform(dateTransformer)
  public createdAt: Date;

  public get createdAtString(): string {
    return DateTime.fromJSDate(this.createdAt).toLocaleString(DateTime.DATETIME_MED);
  }

  constructor(game: Partial<Game>) {
    Object.assign(this, game);
  }
}
