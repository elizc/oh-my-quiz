import { Expose } from 'class-transformer';

export class GameRequest {
  @Expose({ name: 'with', toPlainOnly: true })
  public with?: Array<string>;

  @Expose({ name: 'with_count', toPlainOnly: true })
  public withCount?: Array<string>;

  constructor(model: Partial<GameRequest> = {}) {
    Object.assign(this, model);
  }
}
