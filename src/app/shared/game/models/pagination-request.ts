import { Expose } from 'class-transformer';
import { PaginationRequest } from '@shared/pagination';

export class GamePaginationRequest extends PaginationRequest {
  @Expose({ name: 'is_completed' })
  public isCompleted: number;
}
