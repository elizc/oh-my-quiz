import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { Observable } from 'rxjs';
import { PaginationResponse } from '@shared/pagination';
import { classToPlain, plainToClass, plainToClassFromExist } from 'class-transformer';
import { map } from 'rxjs/operators';
import { Question, QuestionFilters } from './models';
import { QuestionPaginationRequest } from '@shared/question/models/question-pagination-request';

@Injectable()
export class QuestionService {
  constructor(
    private apiService: ApiService
  ) { }

  public search({ filters, ...params }: {
    page?: number,
    perPage?: number,
    filters?: QuestionFilters,
    orderBy?: string,
    desc?: boolean,
    all?: boolean,
    with?: Array<string>
  } = {}): Observable<PaginationResponse<Question>> {
    const request = new QuestionPaginationRequest({ ...filters, ...params });

    return this.apiService
      .get<PaginationResponse<Question>>('/questions', classToPlain<QuestionPaginationRequest>(request))
      .pipe(
        map((response) => {
          return plainToClassFromExist(new PaginationResponse<Question>(Question), response, { groups: ['main'] });
        })
      );
  }

  public create(question: Question): Observable<Question> {
    return this.apiService
      .post('/questions', classToPlain<Question>(question, { groups: ['create'] }))
      .pipe(
        map((response) => plainToClass(Question, response, { groups: ['main'] }))
      );
  }

  public update(question: Question): Observable<Question> {
    return this.apiService
      .put(`/questions/${question.id}`, classToPlain<Question>(question, { groups: ['update'] }))
      .pipe(
        map((response) => plainToClass(Question, response, { groups: ['main'] }))
      );
  }

  public delete(id: number): Observable<void> {
    return this.apiService.delete(`/questions/${id}`);
  }
}
