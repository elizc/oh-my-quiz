import { PaginationRequest } from '@shared/pagination';
import { Expose } from 'class-transformer';

export class QuestionPaginationRequest extends PaginationRequest {
  @Expose({ name: 'quiz_id' })
  public quizId: number;
}
