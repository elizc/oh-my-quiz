export class QuestionFilters {
  public query: string;
  public quizId: number;

  constructor(filters: Partial<QuestionFilters>) {
    Object.assign(this, filters);
  }
}
