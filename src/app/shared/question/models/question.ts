import { Expose, Type } from 'class-transformer';
import { Media } from '@shared/media';

export class Question {
  @Expose({ groups: ['main', 'update'] })
  public id: number;

  @Expose({ name: 'question', groups: ['main', 'create', 'update'] })
  public text: string;

  @Expose({ name: 'answer_a', groups: ['main', 'create', 'update'] })
  public answerA: string;

  @Expose({ name: 'answer_b', groups: ['main', 'create', 'update'] })
  public answerB: string;

  @Expose({ name: 'answer_c', groups: ['main', 'create', 'update'] })
  public answerC: string;

  @Expose({ name: 'answer_d', groups: ['main', 'create', 'update'] })
  public answerD: string;

  @Expose({ name: 'correct_answer', groups: ['main', 'create', 'update'] })
  public correctAnswer: string;

  @Expose({ groups: ['main', 'create', 'update'] })
  public time: number;

  @Expose({ name: 'quiz_id', groups: ['main', 'create', 'update'] })
  public quizId: number;

  @Expose({ name: 'img_id', groups: ['main', 'create', 'update'] })
  public imageID: number = null;

  @Type(() => Media)
  @Expose({ name: 'img', groups: ['main'] })
  public image: Media;

  public get timeInMilliseconds(): number {
    return this.time * 1000;
  }

  constructor(question: Partial<Question>) {
    Object.assign(this, question);
  }
}
