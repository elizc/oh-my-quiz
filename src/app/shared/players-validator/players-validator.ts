import { FormArrayState, ValidationFn } from 'ngrx-forms';

export function playersValidator(control: FormArrayState<string>): ValidationFn<Array<string>> {
  if (!control.value) {
    return (): null => {
      return null;
    };
  }

  if (control.value.find((player) => player !== '')) {
    return (): null => {
      return null;
    };
  }

  return (): { [key: string]: any } => {
    return { players: { value: true } };
  };
}
