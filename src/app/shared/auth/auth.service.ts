import { Injectable } from '@angular/core';
import { ApiService } from '@shared/api';
import { classToPlain, plainToClass } from 'class-transformer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthResponse } from './models';
import { AuthCredentials } from '@shared/auth';
import { User } from '@shared/user';

@Injectable()
export class AuthService {
  constructor(
    private apiService: ApiService
  ) { }

  public authorize(credentials: AuthCredentials): Observable<AuthResponse> {
    return this.apiService
      .post('/login', credentials)
      .pipe(
        map((response) => plainToClass(AuthResponse, response, { groups: ['main'] }))
      );
  }

  public register(user: User): Observable<AuthResponse> {
    return this.apiService
      .post('/register', classToPlain<User>(user, { groups: ['register'] }))
      .pipe(
        map((response) => plainToClass(AuthResponse, response, { groups: ['main'] }))
      );
  }

  public refreshToken(): Observable<{ token: string }> {
    return this.apiService.get('/auth/refresh');
  }
}
