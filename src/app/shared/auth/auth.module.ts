import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JwtHelperService, JwtInterceptor } from '@auth0/angular-jwt';
import { Store } from '@ngrx/store';
import { ApiModule } from '@shared/api/api.module';
import { AuthService } from './auth.service';
import { AuthenticatedGuard, UnauthenticatedGuard } from './guards';
import { TokenRefreshInterceptor } from './interceptors';

@NgModule({
  imports: [
    ApiModule,
    RouterModule
  ],
  providers: [
    AuthService,
    AuthenticatedGuard,
    UnauthenticatedGuard,
    JwtInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenRefreshInterceptor,
      deps: [JwtHelperService, JwtInterceptor, Store],
      multi: true
    }
  ]
})
export class AuthModule { }
