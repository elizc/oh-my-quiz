import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AuthSelectors } from '../store/selectors';
import { map } from 'rxjs/operators';
import { AppState } from '@shared/store';

@Injectable()
export class UnauthenticatedGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  public canActivate(): Observable<boolean> {
    return this.store
      .pipe(
        select(AuthSelectors.isAuthenticated),
        map((isAuthenticated) => {
          if (isAuthenticated) {
            this.router.navigate(['/account']);
          }

          return !isAuthenticated;
        })
      );
  }
}
