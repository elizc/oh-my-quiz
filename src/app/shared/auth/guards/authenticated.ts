import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthSelectors } from '@shared/auth';
import { AppState } from '@shared/store';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  public canActivate(): Observable<boolean> {
    return this.store
      .pipe(
        select(AuthSelectors.isAuthenticated),
        map((isAuthenticated) => {
          if (!isAuthenticated) {
            this.router.navigate(['/login']);
          }

          return isAuthenticated;
        })
      );
  }
}
