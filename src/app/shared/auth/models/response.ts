import { plainToClass, Transform, Expose } from 'class-transformer';
import { User } from '@shared/user/models/user';

export class AuthResponse {
  @Expose({ groups: ['main'] })
  @Transform((user) => plainToClass(User, user, { groups: ['main'] }))
  public user: User;

  @Expose({ groups: ['main'] })
  public token: string;
}
