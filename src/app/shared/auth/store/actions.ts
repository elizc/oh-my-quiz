import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthResponse } from '../models';
import { AuthCredentials } from '@shared/auth';
import { User } from '@shared/user';

export const authActions = {
  updateToken: createAction(
    '[Auth] Update Token',
    props<{ token?: string }>()
  ),
  removeToken: createAction(
    '[Auth] Remove Token'
  ),
  refreshToken: createAction(
    '[Auth] Refresh Token'
  ),
  refreshTokenSuccess: createAction(
    '[Auth] Refresh Token Success',
    props<{ token: string }>()
  ),
  refreshTokenFailure: createAction(
    '[Auth] Refresh Token Failure',
    props<{ response: HttpErrorResponse }>()
  ),
  authorize: createAction(
    '[Auth] Authorize',
    props<{ credentials: AuthCredentials }>()
  ),
  authorizeSuccess: createAction(
    '[Auth] Authorize success',
    props<{ response: AuthResponse }>()
  ),
  authorizeFailure: createAction(
    '[Auth] Authorize failure',
    props<{ response: HttpErrorResponse }>()
  ),
  unauthorize: createAction(
    '[Auth] Unauthorize'
  ),
  register: createAction(
    '[Auth] Register',
    props<{ user: User }>()
  ),
  registerSuccess: createAction(
    '[Auth] Register success',
    props<{ response: AuthResponse }>()
  ),
  registerFailure: createAction(
    '[Auth] Register failure',
    props<{ response: HttpErrorResponse }>()
  )
};
