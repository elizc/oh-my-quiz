import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { authActions } from './actions';
import { AuthService } from '@shared/auth';
import {
  catchError,
  exhaustMap,
  map,
  switchMap,
  tap,
  filter,
  mergeMap
} from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { userActions } from '@shared/user/store/actions';

@Injectable()
export class AuthEffects {
  public authorize$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.authorize),
      exhaustMap((action) => {
        return this.authService
          .authorize(action.credentials)
          .pipe(
            map((response) => authActions.authorizeSuccess({ response })),
            catchError((response) => of(authActions.authorizeFailure({ response })))
          );
      })
    )
  );

  public authSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.authorizeSuccess),
      mergeMap((action) => [
        authActions.updateToken({ token: action.response.token }),
        userActions.updateProfile({ profile: action.response.user })
      ])
    )
  );

  public register$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.register),
      exhaustMap((action) => {
        return this.authService
          .register(action.user)
          .pipe(
            map((response) => authActions.registerSuccess({ response })),
            catchError((response) => of(authActions.registerFailure({ response })))
          );
      })
    )
  );

  public registerSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.registerSuccess),
      mergeMap((action) => [
        authActions.updateToken({ token: action.response.token }),
        userActions.updateProfile({ profile: action.response.user })
      ])
    )
  );

  public unauthorize$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.unauthorize),
      tap(() => this.router.navigate(['/login'])),
      mergeMap(() => [
        authActions.removeToken(),
        userActions.clearProfile()
      ])
    )
  );

  public refreshToken$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.refreshToken),
      switchMap(() => this.authService.refreshToken()),
      map((response) => authActions.refreshTokenSuccess({ token: response.token })),
      catchError((response: HttpErrorResponse) => of(authActions.refreshTokenFailure({ response })))
    )
  );

  public refreshTokenSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.refreshTokenSuccess),
      tap((action) => {
        localStorage.setItem('access_token', action.token);
      })
    ),
    { dispatch: false }
  );

  public updateToken$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.updateToken),
      filter((action) => !!action.token),
      tap((action) => {
        localStorage.setItem('access_token', action.token);
      })
    ),
    { dispatch: false }
  );

  public removeToken$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(authActions.removeToken),
      tap(() => {
        localStorage.removeItem('access_token');
      })
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router
  ) { }
}
