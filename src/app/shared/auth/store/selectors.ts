import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AuthState } from './state';
import { AppState } from '@shared/store';

export const selectFeature = (state: AppState) => state.authState;

export class AuthSelectors {
  public static token: MemoizedSelector<AppState, string> = createSelector(
    selectFeature,
    (state: AuthState) => state.token
  );

  public static isAuthenticated: MemoizedSelector<AppState, boolean> = createSelector(
    AuthSelectors.token,
    (token: string) => !!token
  );

  public static isTokenRefreshing: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AuthState) => state.isTokenRefreshing
  );
}
