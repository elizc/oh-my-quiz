export class AuthState {
  public token?: string;
  public isTokenRefreshing: boolean;

  constructor() {
    this.token = localStorage.getItem('access_token') || null;
    this.isTokenRefreshing = false;
  }
}
