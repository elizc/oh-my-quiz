import { Action, createReducer, on } from '@ngrx/store';
import { AuthState } from './state';
import { authActions } from './actions';

export const initialState = new AuthState();

const reducer = createReducer(
  initialState,
  on(authActions.removeToken, (state) => ({
    ...state,
    token: null
  })),
  on(authActions.updateToken, (state, action) => ({
    ...state,
    token: action.token
  })),
  on(authActions.refreshToken, (state) => ({
    ...state,
    isTokenRefreshing: true
  })),
  on(authActions.refreshTokenSuccess, (state, action) => ({
    ...state,
    token: action.token,
    isTokenRefreshing: false
  })),
  on(authActions.refreshTokenFailure, (state) => ({
    ...state,
    isTokenRefreshing: false
  }))
);

export function authReducer(state: AuthState | undefined, action: Action): AuthState {
  return reducer(state, action);
}
