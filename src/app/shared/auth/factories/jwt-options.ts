import { select, Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { take } from 'rxjs/operators';
import { configuration } from '@configuration';
import { AuthSelectors } from '@shared/auth';

export function jwtOptionsFactory(store: Store<AppState>): object {
  return {
    tokenGetter: () => {
      return store
        .pipe(
          select(AuthSelectors.token),
          take(1)
        )
        .toPromise();
    },
    whitelistedDomains: configuration.api.whitelisted_domains,
    blacklistedRoutes: configuration.api.unauthorized_routes
  };
}
