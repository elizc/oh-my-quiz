import { createSelector, MemoizedSelector } from '@ngrx/store';
import { UserState } from './state';
import { AppState } from '@shared/store';
import { User } from '../models';

const selectFeature = (state: AppState) => state.userState;

export class UserSelectors {
  public static profile: MemoizedSelector<AppState, User> = createSelector(
    selectFeature,
    (state: UserState) => state.profile
  );

  public static language: MemoizedSelector<AppState, string> = createSelector(
    selectFeature,
    (state: UserState) => state.language
  );
}
