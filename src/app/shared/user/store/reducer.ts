import { Action, createReducer, on } from '@ngrx/store';
import { userActions } from './actions';
import { UserState } from './state';

const initialState = new UserState();

const reducer = createReducer(
  initialState,
  on(userActions.updateProfile, (state, action) => ({
    ...state,
    profile: {
      ...state.profile,
      ...action.profile
    }
  })),
  on(userActions.clearProfile, (state) => ({
    ...state,
    profile: null
  })),
  on(userActions.changeLanguage, (state, action) => ({
    ...state,
    language: action.language
  }))
);

export function userReducer(state: UserState | undefined, action: Action): UserState {
  return reducer(state, action);
}
