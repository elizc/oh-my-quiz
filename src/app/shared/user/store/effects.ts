import { Action, Store } from '@ngrx/store';
import {
  Actions,
  createEffect,
  ofType,
  ROOT_EFFECTS_INIT
} from '@ngrx/effects';
import { AppState } from '@shared/store';
import { AuthSelectors } from '@shared/auth';
import {
  catchError,
  filter,
  map,
  switchMap,
  tap,
  withLatestFrom,
  mergeMap
} from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../models';
import { userActions } from './actions';
import { UserService } from '../user.service';

@Injectable()
export class UserEffects {
  public init$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      withLatestFrom(this.store.select(AuthSelectors.isAuthenticated)),
      filter(([_, isAuthenticated]) => isAuthenticated),
      map(() => userActions.refreshProfile())
    )
  );

  public refreshProfile$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(userActions.refreshProfile),
      withLatestFrom(this.store.select(AuthSelectors.isAuthenticated)),
      filter(([_, isAuthenticated]) => isAuthenticated),
      switchMap(() => this.userService.refreshProfile()),
      mergeMap((user: User) => [
        userActions.updateProfile({ profile: user }),
        userActions.refreshProfileSuccess()
      ]),
      catchError((response: HttpErrorResponse) => of(userActions.refreshProfileFailure({ response })))
    )
  );

  public updateProfile$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(userActions.updateProfile),
      tap((action) => {
        localStorage.setItem('user_id', String(action.profile.id));
      })
    ),
    { dispatch: false }
  );

  public clearProfile$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(userActions.clearProfile),
      tap(() => {
        localStorage.removeItem('user_id');
      })
    ),
    { dispatch: false }
  );

  public changeLanguage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(userActions.changeLanguage),
      tap((action) => {
        localStorage.setItem('user_language', action.language);
      })
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private userService: UserService
  ) { }
}
