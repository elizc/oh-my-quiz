import { createAction, props } from '@ngrx/store';
import { User } from '../models';
import { HttpErrorResponse } from '@angular/common/http';

export const userActions = {
  refreshProfile: createAction(
    '[User] Refresh User Profile'
  ),
  refreshProfileSuccess: createAction(
    '[User] Refresh User Profile Success'
  ),
  updateProfile: createAction(
    '[User] Update User Profile',
    props<{ profile: Partial<User> }>()
  ),
  refreshProfileFailure: createAction(
    '[User] Refresh User Profile Failure',
    props<{ response: HttpErrorResponse }>()
  ),
  clearProfile: createAction(
    '[User] Clear'
  ),
  changeLanguage: createAction(
    '[User] Change Language',
    props<{ language: string }>()
  )
};
