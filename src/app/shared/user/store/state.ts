import { configuration } from '@configuration';
import { User } from '../models';
import { plainToClass } from 'class-transformer';

export class UserState {
  public profile: User;
  public language?: string;

  constructor() {
    this.profile = plainToClass(User, {
      id: parseInt(localStorage.getItem('user_id'), 10)
    }, { ignoreDecorators: true });
    this.language = localStorage.getItem('user_language') || configuration.languages.default;
  }
}
