import { Expose } from 'class-transformer';

export class User {
  @Expose({ groups: ['main', 'refreshProfile'] })
  public id: number;

  @Expose({ groups: ['main', 'refreshProfile', 'register'] })
  public name: string;

  @Expose({ groups: ['main', 'refreshProfile', 'register'] })
  public email: string;

  @Expose({ toPlainOnly: true, groups: ['main', 'register'] })
  public password: string;

  @Expose({ toPlainOnly: true, groups: ['main', 'register'] })
  public confirm: string;

  constructor(user: Partial<User>) {
    Object.assign(this, user);
  }
}

