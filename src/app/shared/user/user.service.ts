import { Injectable } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './models';
import { ApiService } from '@shared/api';

@Injectable()
export class UserService {
  constructor(
    private apiService: ApiService
  ) { }

  public refreshProfile(): Observable<User> {
    return this.apiService
      .get('/profile')
      .pipe(
        map((response) => plainToClass(User, response, { groups: ['refreshProfile'] }))
      );
  }
}
