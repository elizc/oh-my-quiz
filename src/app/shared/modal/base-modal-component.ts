import { MatDialogRef } from '@angular/material/dialog';

export class BaseModalComponent <T> {
  constructor(
    private dialogRef: MatDialogRef<T>
  ) {}

  public close(): void {
    this.dialogRef.close();
  }
}
