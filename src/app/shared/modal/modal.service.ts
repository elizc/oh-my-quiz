import { ComponentType } from '@angular/cdk/overlay';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { configuration } from '@configuration';

@Injectable()
export class ModalService {
  constructor(
    private matDialog: MatDialog
  ) { }

  public open<Component, Data = any>(
    component: ComponentType<Component>,
    config: MatDialogConfig<Data> = {}
  ): MatDialogRef<Component> {
    return this.matDialog.open(component, { ...configuration.modal, ...config });
  }

  public closeByID(id: string): void {
    const openedDialog = this.matDialog.getDialogById(id);
    if (openedDialog) {
      openedDialog.close();
    }
  }

  public closeAll(): void {
    this.matDialog.closeAll();
  }
}
