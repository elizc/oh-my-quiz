import { MatDialogModule } from '@angular/material/dialog';
import { ModalService } from './modal.service';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    MatDialogModule
  ],
  providers: [
    ModalService
  ]
})
export class ModalModule { }
