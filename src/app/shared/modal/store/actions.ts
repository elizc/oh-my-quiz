import { ComponentType } from '@angular/cdk/overlay';
import { createAction, props } from '@ngrx/store';
import { MatDialogConfig } from '@angular/material/dialog';

export class ModalActions {
  /* tslint:disable:typedef */
  public static open = createAction(
    '[Modal] Open',
    props<{ component: ComponentType<any>; config?: MatDialogConfig }>()
  );

  public static closeByID = createAction(
    '[Modal] Close by ID',
    props<{ modalID: string }>()
  );

  public static closeAll = createAction(
    '[Modal] Close all'
  );
  /* tslint:enable:typedef */
}
