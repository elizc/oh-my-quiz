import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { ModalActions } from './actions';
import { ModalService } from '../modal.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ModalEffects {
  public open$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ModalActions.open),
      tap((action) => this.modalService.open(action.component, action.config))
    ),
    { dispatch: false }
  );

  public closeByID$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ModalActions.closeByID),
      tap((action) => this.modalService.closeByID(action.modalID))
    ),
    { dispatch: false }
  );

  public closeAll$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ModalActions.closeAll),
      tap(() => this.modalService.closeAll())
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private modalService: ModalService
  ) { }
}
