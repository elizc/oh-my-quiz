import { MediaService } from './media.service';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [
    MediaService
  ]
})
export class MediaModule { }
