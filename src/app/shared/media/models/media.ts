import { Expose, Transform } from 'class-transformer';
import { dateTransformer } from '@shared/date-transformer';

export class Media {
  @Expose({ groups: ['main', 'create', 'update', 'refreshProfile'] })
  public id: number;

  @Transform(dateTransformer)
  @Expose({ name: 'created_at', groups: ['main', 'refreshProfile'] })
  public createdAt: Date;

  @Transform(dateTransformer)
  @Expose({ name: 'updated_at', groups: ['main', 'refreshProfile'] })
  public updatedAt?: Date;

  @Transform(dateTransformer)
  @Expose({ name: 'deleted_at', groups: ['main', 'refreshProfile'] })
  public deletedAt?: Date;

  @Expose({ groups: ['main', 'create', 'update', 'refreshProfile'] })
  public link: string;

  @Expose({ groups: ['main', 'create', 'update', 'refreshProfile'] })
  public name: string;

  @Expose({ name: 'is_public', groups: ['main', 'refreshProfile'] })
  public isPublic: boolean;

  @Expose({ name: 'owner_id', groups: ['main', 'refreshProfile'] })
  public ownerID: number;

  constructor(model: Partial<Media> = {}) {
    Object.assign(this, model);
  }
}
