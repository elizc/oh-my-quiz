export class MediaFilters {
  public query: string;
  public id: number;

  constructor(filters: Partial<MediaFilters>) {
    Object.assign(this, filters);
  }
}
