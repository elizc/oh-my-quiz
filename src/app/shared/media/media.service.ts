import { ApiService } from '../api/api.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Media } from './models';
import { Observable } from 'rxjs';
import { plainToClass } from 'class-transformer';

@Injectable()
export class MediaService {
  constructor(
    private apiService: ApiService
  ) { }

  public create(file: File, options: { isPublic: boolean } = { isPublic: true }): Observable<Media> {
    return this.apiService
      .post<Media>('/media', { file, is_public: (options.isPublic) ? '1' : '0' })
      .pipe(
        map((response) => plainToClass(Media, { ...response, type: file.type }, { groups: ['create'] }))
      );
  }
}
