import { MediaModule } from './media.module';
import { MediaService } from './media.service';

export * from './models';

export {
  MediaService,
  MediaModule
};
