import { ApiModule } from './api.module';
import { ApiService } from './api.service';

export * from './interceptors';
export {
  ApiModule,
  ApiService
};
