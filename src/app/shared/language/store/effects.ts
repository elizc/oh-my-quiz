import { Action, Store } from '@ngrx/store';
import {
  Actions,
  createEffect,
  ofType,
  ROOT_EFFECTS_INIT
} from '@ngrx/effects';
import { AppState } from '@shared/store';
import { AuthSelectors } from '@shared/auth';
import { configuration } from '@configuration';
import { includes } from 'lodash';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, withLatestFrom } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Settings } from 'luxon';

@Injectable()
export class LanguageEffects {
  public initLanguage$: Observable<[Action, boolean]> = createEffect(
    () => this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      withLatestFrom(this.store.select(AuthSelectors.isAuthenticated)),
      tap(([_, isAuthenticated]) => {
        const storedLanguage = window.localStorage.getItem('user_language');
        const language = (isAuthenticated) ? storedLanguage : this.translateService.getBrowserLang();

        this.translateService.addLangs(configuration.languages.available);
        this.setLanguage(language);
      })
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private translateService: TranslateService
  ) {}

  private setLanguage(language: string): void {
    if (this.isLanguageAvailable(language)) {
      this.translateService.setDefaultLang(language);
      Settings.defaultLocale = language;
    } else {
      this.setDefaultLanguage();
    }
  }

  private setDefaultLanguage(): void {
    this.setLanguage(configuration.languages.default);
    Settings.defaultLocale = configuration.languages.default;
  }

  private isLanguageAvailable(language: string): boolean {
    return includes(configuration.languages.available, language);
  }
}
