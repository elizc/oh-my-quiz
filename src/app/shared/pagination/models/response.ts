import { Expose, Type, Exclude } from 'class-transformer';

export class PaginationResponse<T> {
  @Expose({ name: 'current_page' })
  public currentPage: number;

  @Expose({ name: 'per_page' })
  public perPage: number;

  public total: number;

  @Type((options) => {
    return (options.newObject as PaginationResponse<T>).type;
  })
  public data: Array<T>;

  @Exclude()
  private type: Function;

  constructor(type: Function) {
    this.type = type;
  }
}
