import { PaginationResponse } from './response';
import { PaginationRequest } from './request';

export {
  PaginationRequest,
  PaginationResponse
};
