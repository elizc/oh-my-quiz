import { Expose } from 'class-transformer';

export class PaginationRequest {
  @Expose({ toPlainOnly: true })
  public page?: number;

  @Expose({ name: 'per_page', toPlainOnly: true })
  public perPage?: number;

  @Expose({ toPlainOnly: true })
  public all?: boolean;

  @Expose({ toPlainOnly: true })
  public query?: string;

  @Expose({ name: 'order_by', toPlainOnly: true })
  public orderBy?: string;

  @Expose({ toPlainOnly: true })
  public desc?: boolean;

  @Expose({ name: 'with', toPlainOnly: true })
  public with?: Array<string>;

  @Expose({ name: 'with_count', toPlainOnly: true })
  public withCount?: Array<string>;

  constructor(model: Partial<PaginationRequest> = {}) {
    Object.assign(this, model);
  }
}
