import { Action, createReducer, on } from '@ngrx/store';
import { PresenterActions } from './actions';
import { PresenterState } from './state';
import { GameStage } from '@shared/game';

const initialState = new PresenterState();

const reducer = createReducer(
  initialState,
  on(PresenterActions.resetState, () => initialState),
  on(PresenterActions.startGame, (state) => ({
    ...state,
    isStartingGame: true
  })),
  on(PresenterActions.requestNextQuestion, (state) => ({
    ...state,
    isLoading: true
  })),
  on(PresenterActions.nextQuestionReceived, (state) => ({
    ...state,
    isLoading: false
  })),
  on(PresenterActions.finishGame, (state) => ({
    ...state,
    isLoading: true
  })),
  on(PresenterActions.gameFinished, (state) => ({
    ...state,
    isLoading: false
  })),
  on(PresenterActions.setGame, (state, action) => ({
    ...initialState,
    game: action.game
  })),
  on(PresenterActions.connected, (state) => ({
    ...state,
    stage: GameStage.WAIT
  })),
  on(PresenterActions.teamJoined, (state, action) => ({
    ...state,
    teams: action.teams
  })),
  on(PresenterActions.gameStarted, (state, action) => ({
    ...state,
    stage: GameStage.GET_READY,
    questionsCount: action.questionsCount,
    isStartingGame: false
  })),
  on(PresenterActions.setGetReadyTimer, (state, action) => ({
    ...state,
    getReadyTimer: action.seconds
  })),
  on(PresenterActions.nextQuestionReceived, (state, action) => ({
    ...state,
    stage: GameStage.QUESTION,
    question: action.question,
    currentQuestionNumber: state.currentQuestionNumber + 1,
    correctAnswer: null,
    gameQuestionId: action.gameQuestionId
  })),
  on(PresenterActions.setQuestionTimer, (state, action) => ({
    ...state,
    questionTimer: action.seconds
  })),
  on(PresenterActions.questionResultsReceived, (state, action) => ({
    ...state,
    teams: action.teams,
    correctAnswer: action.correctAnswer
  })),
  on(PresenterActions.showQuestionResults, (state) => ({
    ...state,
    stage: GameStage.QUESTION_RESULTS
  })),
  on(PresenterActions.gameFinished, (state, action) => ({
    ...state,
    stage: GameStage.FINAL_RESULTS,
    teams: action.teams
  }))
);

export function presenterReducer(state: PresenterState | undefined, action: Action): PresenterState {
  return reducer(state, action);
}
