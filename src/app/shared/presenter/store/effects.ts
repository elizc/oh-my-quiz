import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of, interval, race } from 'rxjs';
import {
  catchError,
  delay,
  exhaustMap,
  filter,
  map,
  skip,
  switchMap,
  take,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import { Game, GameService } from '@shared/game';
import { EchoService } from '@shared/echo';
import { PresenterActions, PresenterSelectors } from '@shared/presenter';
import { Router } from '@angular/router';
import { Team, TeamService } from '@shared/team';
import { classToPlain, plainToClass } from 'class-transformer';
import { Question } from '@shared/question';
import { isNumber } from 'lodash';

@Injectable()
export class PresenterEffects {
  public init$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.init),
      withLatestFrom(this.store.select(PresenterSelectors.game)),
      filter(([_, game]) => !!game),
      map(() => PresenterActions.tryConnect())
    )
  );

  public setGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.setGame),
      tap((action) => localStorage.setItem('game', JSON.stringify(classToPlain<Game>(action.game, { groups: ['main'] }))))
    ),
    { dispatch: false }
  );

  public startGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.startGame),
      withLatestFrom(this.store.select(PresenterSelectors.game)),
      exhaustMap(([_, game]) => this.gameService
        .start(game.id)
        .pipe(
          map((response) => PresenterActions.startGameSuccess({ response })),
          catchError((response) => of(PresenterActions.startGameFailure({ response })))
        )
      )
    )
  );

  public tryConnect$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.tryConnect),
      tap(() => {
        this.echoService.init();

        this.echoService.server.connector.socket.on('connect', () => this.store.dispatch(PresenterActions.connected()));
        this.echoService.server.connector.socket.on('disconnect', () => this.store.dispatch(PresenterActions.disconnected()));
      })
    ),
    { dispatch: false }
  );

  public connect$: Observable<[Action, Game]> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.connected),
      withLatestFrom(this.store.select(PresenterSelectors.game)),
      tap(([_, game]) => {
        const channel = this.echoService.server.channel(`Game ${game.id}`);

        channel.listen('TeamJoin', (event) => {
          const teams = event.allTeams.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          this.store.dispatch(PresenterActions.teamJoined({ teams }));
        });

        channel.listen('GameStart', (event) => {
          this.store.dispatch(PresenterActions.gameStarted({ questionsCount: event.game.game_questions_count }));
        });

        channel.listen('NextQuestion', (event) => {
          const question = plainToClass(Question, event.question.question, { groups: ['main'] });
          const gameQuestionId = event.question.id;

          this.store.dispatch(PresenterActions.nextQuestionReceived({ question, gameQuestionId }));
        });

        channel.listen('ResultsData', (event) => {
          const teams = event.teamScore.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          const correctAnswer = event.correctAnswer;

          this.store.dispatch(PresenterActions.questionResultsReceived({ teams, correctAnswer }));
        });

        channel.listen('GameFinish', (event) => {
          const teams = event.teams.map((team) => plainToClass(Team, team, { groups: ['main'] }));
          this.store.dispatch(PresenterActions.gameFinished({ teams }));
        });
      })
    ),
    { dispatch: false }
  );

  public gameStarted$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.gameStarted),
      map(() => PresenterActions.startGetReadyTimer())
    )
  );

  public startGetReadyTimer$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.startGetReadyTimer),
      tap(() => this.store.dispatch(PresenterActions.setGetReadyTimer({ seconds: 3 }))),
      delay(1000),
      tap(() => this.store.dispatch(PresenterActions.setGetReadyTimer({ seconds: 2 }))),
      delay(1000),
      tap(() => this.store.dispatch(PresenterActions.setGetReadyTimer({ seconds: 1 }))),
      delay(1000),
      tap(() => this.store.dispatch(PresenterActions.setGetReadyTimer({ seconds: null }))),
      map(() => PresenterActions.requestNextQuestion())
    )
  );

  public nextQuestionReceived$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.nextQuestionReceived),
      map(() => PresenterActions.startQuestionTimer())
    )
  );

  public startQuestionTimer$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.startQuestionTimer),
      withLatestFrom(this.store.select(PresenterSelectors.question)),
      switchMap(([_, question]) => race(
        interval(1000).pipe(
          take(question.time + 1),
          tap((count) => {
            this.store.dispatch(PresenterActions.setQuestionTimer({ seconds: question.time - count }));
          }),
          skip(question.time)
        ),
        this.actions$.pipe(ofType(PresenterActions.questionResultsReceived))
      )),
      filter((value) => isNumber(value)),
      map(() => PresenterActions.requestQuestionResults())
    )
  );

  public questionResultsReceived$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.questionResultsReceived),
      delay(5000),
      map(() => PresenterActions.showQuestionResults())
    )
  );

  public requestNextQuestion$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.requestNextQuestion),
      withLatestFrom(this.store.select(PresenterSelectors.game)),
      exhaustMap(([_, game]) => {
        return this.gameService.nextQuestion(game.id)
          .pipe(
            map((response) => PresenterActions.requestNextQuestionSuccess({ response })),
            catchError((response) => of(PresenterActions.requestNextQuestionFailure({ response })))
          );
      })
    )
  );

  public requestQuestionResults$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.requestQuestionResults),
      withLatestFrom(
        this.store.select(PresenterSelectors.game),
        this.store.select(PresenterSelectors.gameQuestionId)
      ),
      exhaustMap(([_, game, gameQuestionId]) => {
        return this.gameService.questionResults(game.id, gameQuestionId)
          .pipe(
            map((response) => PresenterActions.requestQuestionResultsSuccess({ response })),
            catchError((response) => of(PresenterActions.requestQuestionResultsFailure({ response })))
          );
      })
    )
  );

  public finishGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.finishGame),
      withLatestFrom(this.store.select(PresenterSelectors.game)),
      exhaustMap(([_, game]) => this.gameService
        .finish(game.id)
        .pipe(
          map((response) => PresenterActions.finishGameSuccess({ response })),
          catchError((response) => of(PresenterActions.finishGameFailure({ response })))
        )
      )
    )
  );

  public quit$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PresenterActions.quit),
      map(() => PresenterActions.resetState()),
      tap(() => this.router.navigate(['/account/quizes']))
    )
  );

  constructor(
    private actions$: Actions,
    private gameService: GameService,
    private echoService: EchoService,
    private teamService: TeamService,
    private router: Router,
    private store: Store<AppState>
  ) { }
}
