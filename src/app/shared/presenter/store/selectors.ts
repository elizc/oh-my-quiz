import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PresenterState } from './state';
import { Game, GameStage } from '@shared/game';
import { Question } from '@shared/question';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';

const selectFeature = (state: AppState) => state.presenter;

export class PresenterSelectors {
  public static isStartingGame: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PresenterState) => state.isStartingGame
  );

  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
      selectFeature,
      (state: PresenterState) => state.isLoading
    );

  public static game: MemoizedSelector<AppState, Game> = createSelector(
    selectFeature,
    (state: PresenterState) => state.game
  );

  public static quiz: MemoizedSelector<AppState, Quiz> = createSelector(
    PresenterSelectors.game,
    (game: Game) => game.quiz
  );

  public static question: MemoizedSelector<AppState, Question> = createSelector(
    selectFeature,
    (state: PresenterState) => state.question
  );

  public static questionsCount: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PresenterState) => state.questionsCount
  );

  public static currentQuestionNumber: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PresenterState) => state.currentQuestionNumber
  );

  public static stage: MemoizedSelector<AppState, GameStage> = createSelector(
    selectFeature,
    (state: PresenterState) => state.stage
  );

  public static teams: MemoizedSelector<AppState, Array<Team>> = createSelector(
    selectFeature,
    (state: PresenterState) => state.teams
  );

  public static getReadyTimer: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PresenterState) => state.getReadyTimer
  );

  public static questionTimer: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PresenterState) => state.questionTimer
  );

  public static formattedQuestionTimer: MemoizedSelector<AppState, string> = createSelector(
    PresenterSelectors.questionTimer,
    (questionTimer: number) => {
      const minutes = Math.floor(questionTimer / 60);
      const seconds = questionTimer % 60;

      return `${minutes}:${(seconds < 10) ? '0' : ''}${seconds}`;
    }
  );

  public static correctAnswer: MemoizedSelector<AppState, string> = createSelector(
    selectFeature,
    (state: PresenterState) => state.correctAnswer
  );

  public static gameQuestionId: MemoizedSelector<AppState, number> = createSelector(
    selectFeature,
    (state: PresenterState) => state.gameQuestionId
  );
}
