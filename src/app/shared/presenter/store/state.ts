import { Game, GameStage } from '@shared/game';
import { Question } from '@shared/question';
import { plainToClass } from 'class-transformer';
import { Team } from '@shared/team';

export class PresenterState {
  public isStartingGame: boolean;
  public isLoading: boolean;
  public game: Game;
  public question: Question;
  public stage: GameStage;
  public teams: Array<Team>;
  public getReadyTimer: number;
  public questionsCount: number;
  public currentQuestionNumber: number;
  public questionTimer: number;
  public correctAnswer: string;
  public gameQuestionId: number;

  constructor() {
    const game = localStorage.getItem('game');
    this.game = plainToClass(Game, JSON.parse(game), { groups: ['main'] });

    this.isStartingGame = false;
    this.isLoading = false;
    this.question = null;
    this.stage = null;
    this.teams = [];
    this.getReadyTimer = null;
    this.questionsCount = null;
    this.currentQuestionNumber = 0;
    this.questionTimer = null;
    this.correctAnswer = null;
    this.gameQuestionId = null;
  }
}
