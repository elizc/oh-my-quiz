import { createAction, props } from '@ngrx/store';
import { Game } from '@shared/game';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Team } from '@shared/team';
import { Question } from '@shared/question';

export class PresenterActions {
  /* tslint:disable:typedef */
  public static init = createAction(
    '[PresenterActions] Init'
  );

  public static resetState = createAction(
    '[PresenterActions] Reset State'
  );

  public static setGame = createAction(
    '[PresenterActions] Set Game',
    props<{ game: Game }>()
  );

  public static loadGame = createAction(
    '[PresenterActions] Load Game',
    props<{ id: number }>()
  );

  public static loadGameSuccess = createAction(
    '[PresenterActions] Load Game Success',
    props<{ game: Game }>()
  );

  public static loadGameFailure = createAction(
    '[PresenterActions] Load Game Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static startGame = createAction(
    '[PresenterActions] Start Game'
  );

  public static startGameSuccess = createAction(
    '[PresenterActions] Start Game Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static startGameFailure = createAction(
    '[PresenterActions] Start Game Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static tryConnect = createAction(
    '[PresenterActions] Try Connect'
  );

  public static connected = createAction(
    '[PresenterActions] Connected'
  );

  public static disconnected = createAction(
    '[PresenterActions] Disconnected'
  );

  public static teamJoined = createAction(
    '[PresenterActions] Team Joined',
    props<{ teams: Array<Team> }>()
  );

  public static startGetReadyTimer = createAction(
    '[PresenterActions] Start Get Ready Timer'
  );

  public static setGetReadyTimer = createAction(
    '[PresenterActions] Set Get Ready Timer',
    props<{ seconds: number }>()
  );

  public static startQuestionTimer = createAction(
    '[PresenterActions] Start Question Timer'
  );

  public static setQuestionTimer = createAction(
    '[PresenterActions] Set Question Timer',
    props<{ seconds: number }>()
  );

  public static showQuestionResults = createAction(
    '[PresenterActions] Show Question Results'
  );

  public static finishGame = createAction(
    '[PresenterActions] Finish Game'
  );

  public static finishGameSuccess = createAction(
    '[PresenterActions] Finish Game Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static finishGameFailure = createAction(
    '[PresenterActions] Finish Game Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static gameStarted = createAction(
    '[PresenterActions] Game Started',
    props<{ questionsCount: number }>()
  );

  public static requestNextQuestion = createAction(
    '[PresenterActions] Request Next Question'
  );

  public static requestNextQuestionSuccess = createAction(
    '[PresenterActions] Request Next Question Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static requestNextQuestionFailure = createAction(
    '[PresenterActions] Request Next Question Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static requestQuestionResults = createAction(
    '[PresenterActions] Request Question Results'
  );

  public static requestQuestionResultsSuccess = createAction(
    '[PresenterActions] Request Question Results Success',
    props<{ response: HttpResponse<any> }>()
  );

  public static requestQuestionResultsFailure = createAction(
    '[PresenterActions] Request Question Results Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static nextQuestionReceived = createAction(
    '[PresenterActions] Next Question Received',
    props<{ question: Question, gameQuestionId: number }>()
  );

  public static questionResultsReceived = createAction(
    '[PresenterActions] Question Results Received',
    props<{ teams: Array<Team>, correctAnswer: string }>()
  );

  public static gameFinished = createAction(
    '[PresenterActions] Game Finished',
    props<{ teams: Array<Team> }>()
  );

  public static quit = createAction(
    '[PresenterActions] Quit'
  );
  /* tslint:enable:typedef */
}
