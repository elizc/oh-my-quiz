import { NgModule } from '@angular/core';
import { HasPresenterGameGuard } from '@shared/presenter';

@NgModule({
  providers: [
    HasPresenterGameGuard
  ]
})
export class PresenterModule { }
