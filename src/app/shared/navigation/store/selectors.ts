import { createFeatureSelector, MemoizedSelector, createSelector, MemoizedSelectorWithProps } from '@ngrx/store';
import { RouterReducerState, getSelectors } from '@ngrx/router-store';
import { AppState } from '@shared/store';

export class NavigationSelectors {
  public static routerState: MemoizedSelector<AppState, RouterReducerState> = createFeatureSelector<
    AppState,
    RouterReducerState
  >('router');

  public static url: MemoizedSelector<AppState, string> = createSelector(
    (state: AppState) => state,
    (state: AppState) => selectors.selectUrl(state)
  );

  public static routeParam: MemoizedSelectorWithProps<AppState, string, string> = createSelector(
    (state: AppState) => state,
    (state: AppState, param: string) => selectors.selectRouteParam(param)(state)
  );

  public static queryParam: MemoizedSelectorWithProps<AppState, string, string> = createSelector(
    (state: AppState) => state,
    (state: AppState, param: string) => selectors.selectQueryParam(param)(state)
  );
}

const selectors = getSelectors(NavigationSelectors.routerState);
