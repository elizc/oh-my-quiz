import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { GameStage } from '@shared/game';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterSelectors } from '@shared/presenter';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'app-presenter-root',
  templateUrl: 'presenter.html',
  styleUrls: ['presenter.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterComponent {
  public quiz$: Observable<Quiz>;
  public stage$: Observable<GameStage>;
  public gameStage: typeof GameStage;

  constructor(private store: Store<AppState>) {
    this.quiz$ = this.store.select(PresenterSelectors.quiz);
    this.stage$ = this.store.select(PresenterSelectors.stage);
    this.gameStage = GameStage;
  }
}
