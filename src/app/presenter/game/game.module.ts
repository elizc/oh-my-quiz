import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PresenterGamePageComponent } from './game.component';
import { PresenterGamePageRoutingModule } from './game.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { presenterGamePageReducer, PresenterGamePageEffects } from './shared/store';
import { PresenterGameResultsComponent } from '@app/presenter/game/shared/components/results/results.component';
import { PresenterGameReadyComponent } from '@app/presenter/game/shared/components/ready/ready.component';
import { PresenterGameQuestionComponent } from '@app/presenter/game/shared/components/question/question.component';
import { PresenterGameFinalResultsComponent } from '@app/presenter/game/shared/components/final-results/final-results.component';
import { PresenterGameWaitComponent } from '@app/presenter/game/shared/components/wait/wait.component';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  declarations: [
    PresenterGamePageComponent,
    PresenterGameReadyComponent,
    PresenterGameQuestionComponent,
    PresenterGameResultsComponent,
    PresenterGameFinalResultsComponent,
    PresenterGameWaitComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    QRCodeModule,
    PresenterGamePageRoutingModule,
    StoreModule.forFeature('presenterGamePage', presenterGamePageReducer),
    EffectsModule.forFeature([PresenterGamePageEffects])
  ],
  providers: []
})
export class PresenterGamePageModule { }
