import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GameStage } from '@shared/game';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterActions, PresenterSelectors } from '@shared/presenter';

@Component({
  selector: 'presenter-game-page',
  templateUrl: 'game.html',
  styleUrls: ['game.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGamePageComponent implements OnInit {
  public stage$: Observable<GameStage>;
  public gameStage: typeof GameStage;

  constructor(private store: Store<AppState>) {
    this.stage$ = this.store.select(PresenterSelectors.stage);
    this.gameStage = GameStage;
  }

  public ngOnInit(): void {
    this.store.dispatch(PresenterActions.init());
  }
}
