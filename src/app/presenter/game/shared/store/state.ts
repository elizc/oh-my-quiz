export class PresenterGamePageState {
  public isLoading: boolean;

  constructor() {
    this.isLoading = false;
  }
}
