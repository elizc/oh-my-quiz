import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PresenterGamePageState } from '@app/presenter/game/shared/store/state';

const selectFeature = (state: AppState) => state.presenterGamePage;

export class PresenterGamePageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PresenterGamePageState) => state.isLoading
  );
}
