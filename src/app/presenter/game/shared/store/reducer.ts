import { Action, createReducer, on } from '@ngrx/store';
import { PresenterGamePageActions } from './actions';
import { PresenterGamePageState } from './state';

const initialState = new PresenterGamePageState();

const reducer = createReducer(
  initialState,
  on(PresenterGamePageActions.resetState, () => initialState)
);

export function presenterGamePageReducer(state: PresenterGamePageState | undefined, action: Action): PresenterGamePageState {
  return reducer(state, action);
}
