import { createAction } from '@ngrx/store';

export class PresenterGamePageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PresenterGamePage] Reset State'
  );
  /* tslint:enable:typedef */
}
