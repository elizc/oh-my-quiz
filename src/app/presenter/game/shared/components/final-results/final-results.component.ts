import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterActions, PresenterSelectors } from '@shared/presenter';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'final-results',
  templateUrl: 'final-results.html',
  styleUrls: ['final-results.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGameFinalResultsComponent {
  public teams$: Observable<Array<Team>>;
  public quiz$: Observable<Quiz>;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PresenterSelectors.teams);
    this.quiz$ = this.store.select(PresenterSelectors.quiz);
  }

  public quit(): void {
    this.store.dispatch(PresenterActions.quit());
  }
}
