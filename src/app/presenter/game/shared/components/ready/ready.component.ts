import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '@shared/store';
import { PresenterSelectors } from '@shared/presenter';

@Component({
  selector: 'ready',
  templateUrl: 'ready.html',
  styleUrls: ['ready.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGameReadyComponent {
  public getReadyTimer$: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.getReadyTimer$ = this.store.select(PresenterSelectors.getReadyTimer);
  }
}
