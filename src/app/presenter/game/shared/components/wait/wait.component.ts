import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterActions, PresenterSelectors } from '@shared/presenter';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';
import { Game } from '@shared/game';
import { configuration } from '@configuration';

@Component({
  selector: 'wait',
  templateUrl: 'wait.html',
  styleUrls: ['wait.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGameWaitComponent {
  public teams$: Observable<Array<Team>>;
  public game$: Observable<Game>;
  public quiz$: Observable<Quiz>;
  public isStartingGame$: Observable<boolean>;
  public appUrl: string;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PresenterSelectors.teams);
    this.game$ = this.store.select(PresenterSelectors.game);
    this.quiz$ = this.store.select(PresenterSelectors.quiz);
    this.isStartingGame$ = this.store.select(PresenterSelectors.isStartingGame);
    this.appUrl = configuration.appUrl;
  }

  public start(): void {
    this.store.dispatch(PresenterActions.startGame());
  }
}
