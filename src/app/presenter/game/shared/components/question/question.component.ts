import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Question } from '@shared/question';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterSelectors } from '@shared/presenter';
import { PresenterGamePageSelectors } from '@app/presenter/game/shared/store';

@Component({
  selector: 'question',
  templateUrl: 'question.html',
  styleUrls: ['question.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGameQuestionComponent {
  public question$: Observable<Question>;
  public questionTimer$: Observable<number>;
  public formattedQuestionTimer$: Observable<string>;
  public questionsCount$: Observable<number>;
  public currentQuestionNumber$: Observable<number>;
  public correctAnswer$: Observable<string>;

  constructor(private store: Store<AppState> ) {
    this.question$ = this.store.select(PresenterSelectors.question);
    this.questionsCount$ = this.store.select(PresenterSelectors.questionsCount);
    this.currentQuestionNumber$ = this.store.select(PresenterSelectors.currentQuestionNumber);
    this.questionTimer$ = this.store.select(PresenterSelectors.questionTimer);
    this.formattedQuestionTimer$ = this.store.select(PresenterSelectors.formattedQuestionTimer);
    this.correctAnswer$ = this.store.select(PresenterSelectors.correctAnswer);
  }
}
