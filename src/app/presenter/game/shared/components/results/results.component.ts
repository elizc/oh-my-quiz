import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PresenterActions, PresenterSelectors } from '@shared/presenter';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';
import { PresenterGamePageSelectors } from '@app/presenter/game/shared/store';

@Component({
  selector: 'results',
  templateUrl: 'results.html',
  styleUrls: ['results.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresenterGameResultsComponent {
  public teams$: Observable<Array<Team>>;
  public quiz$: Observable<Quiz>;
  public questionsCount$: Observable<number>;
  public currentQuestionNumber$: Observable<number>;
  public isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PresenterSelectors.teams);
    this.quiz$ = this.store.select(PresenterSelectors.quiz);
    this.questionsCount$ = this.store.select(PresenterSelectors.questionsCount);
    this.currentQuestionNumber$ = this.store.select(PresenterSelectors.currentQuestionNumber);
    this.isLoading$ = this.store.select(PresenterSelectors.isLoading);
  }

  public next(): void {
    this.store.dispatch(PresenterActions.requestNextQuestion());
  }

  public finish(): void {
    this.store.dispatch(PresenterActions.finishGame());
  }
}
