import { PresenterComponent } from './presenter.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PresenterComponent,
    children: [
      {
        path: 'game',
        loadChildren: () => import('./game/game.module').then((module) => module.PresenterGamePageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PresenterRoutingModule { }
