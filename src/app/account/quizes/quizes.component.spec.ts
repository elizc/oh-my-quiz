import { AccountQuizesPageComponent } from './quizes.component';
import { AccountQuizesPageEffects, accountQuizesPageReducer } from './shared/store';
import { AppState } from '@shared/store/state';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { render, RenderResult } from '@testing-library/angular';
import { configuration } from '@configuration';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('AccountQuizesPageComponent', () => {
  let component: RenderResult<AccountQuizesPageComponent>;
  let componentInstance: AccountQuizesPageComponent;
  let store: Store<AppState>;

  const translation = require(`../../../assets/i18n/${configuration.languages.default}.json`);

  beforeEach(async () => {
    component = await render(AccountQuizesPageComponent, {
      imports: [
        TranslateTestingModule.withTranslations(configuration.languages.default, translation),
        StoreModule.forRoot({}, {
          runtimeChecks: {
            strictStateImmutability: true,
            strictActionImmutability: true
          }
        }),
        StoreModule.forFeature('accountQuizesPage', accountQuizesPageReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([AccountQuizesPageEffects])
      ],
      declarations: [
        AccountQuizesPageComponent
      ],
      routes: [],
      providers: []
    });

    componentInstance = component.fixture.componentInstance;
    store = TestBed.inject(Store);
  });

  it('should create', async () => {
    expect(component).toBeDefined();
  });
});

