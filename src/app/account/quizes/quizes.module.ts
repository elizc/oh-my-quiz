import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountQuizesPageComponent } from './quizes.component';
import { AccountQuizesPageRoutingModule } from './quizes.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { accountQuizesPageReducer, AccountQuizesPageEffects } from './shared/store';
import { QuizItemComponent, GameItemComponent } from '@app/account/quizes/shared/components';
import { PluralFormModule } from '@shared/plural-form';
import { AccountQuizesEditModalModule } from '@app/account/quizes/shared/modals/edit-modal/edit-modal.module';
import { QuizModule } from '@shared/quiz';

@NgModule({
  declarations: [
    AccountQuizesPageComponent,
    QuizItemComponent,
    GameItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    PluralFormModule,
    AccountQuizesPageRoutingModule,
    AccountQuizesEditModalModule,
    StoreModule.forFeature('accountQuizesPage', accountQuizesPageReducer),
    EffectsModule.forFeature([AccountQuizesPageEffects]),
    QuizModule
  ],
  providers: []
})
export class AccountQuizesPageModule { }
