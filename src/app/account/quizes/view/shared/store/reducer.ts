import { Action, createReducer, on } from '@ngrx/store';
import { AccountQuizesViewPageActions } from './actions';
import { AccountQuizesViewPageState } from './state';

const initialState = new AccountQuizesViewPageState();

const reducer = createReducer(
  initialState,
  on(AccountQuizesViewPageActions.resetState, () => initialState),
  on(AccountQuizesViewPageActions.loadQuiz, (state) => ({
    ...state,
    isLoading: true
  })),
  on(AccountQuizesViewPageActions.loadQuizSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    quiz: action.quiz
  })),
  on(AccountQuizesViewPageActions.loadQuizFailure, (state, action) => ({
    ...state,
    isLoading: false,
    quizErrorResponse: action.response
  })),
  on(AccountQuizesViewPageActions.loadQuestions, (state) => ({
    ...state,
    isLoading: true
  })),
  on(AccountQuizesViewPageActions.loadQuestionsSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    questions: action.response.data
  })),
  on(AccountQuizesViewPageActions.loadQuestionsFailure, (state, action) => ({
    ...state,
    isLoading: false,
    questionsErrorResponse: action.response
  })),
  on(AccountQuizesViewPageActions.addQuestion, (state, action) => ({
      ...state,
      questions: [...state.questions, action.question]
  })),
  on(AccountQuizesViewPageActions.updateQuestion, (state, action) => {
    const { oldQuestionId, updatedQuestion } = action;

    return {
      ...state,
      questions: [
        ...state.questions.filter((question) => question.id !== oldQuestionId),
        updatedQuestion
      ]
    };
  }),
  on(AccountQuizesViewPageActions.deleteQuestion, (state) => ({
    ...state,
    isDeletingQuestion: true
  })),
  on(AccountQuizesViewPageActions.deleteQuestionSuccess, (state, action) => ({
    ...state,
    isDeletingQuestion: false,
    questions: state.questions.filter((question) => question.id !== action.id)
  })),
  on(AccountQuizesViewPageActions.deleteQuestionFailure, (state, action) => ({
    ...state,
    isDeletingQuestion: false,
    questionsErrorResponse: action.response
  }))
);

export function accountQuizesViewPageReducer(state: AccountQuizesViewPageState | undefined, action: Action): AccountQuizesViewPageState {
  return reducer(state, action);
}
