import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { Quiz } from '@shared/quiz';
import { Question } from '@shared/question';
import { PaginationResponse } from '@shared/pagination';

export class AccountQuizesViewPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[AccountQuizesViewPage] Reset State'
  );

  public static initPage = createAction(
    '[AccountQuizesViewPage] Init Page'
  );

  public static loadQuiz = createAction(
    '[AccountQuizesViewPage] Load Quiz',
    props<{ quizId: number }>()
  );

  public static loadQuizSuccess = createAction(
    '[AccountQuizesViewPage] Load Quiz Success',
    props<{ quiz: Quiz }>()
  );

  public static loadQuizFailure = createAction(
    '[AccountQuizesViewPage] Load Quiz Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static loadQuestions = createAction(
    '[AccountQuizesViewPage] Load Questions',
    props<{ quizId: number }>()
  );

  public static loadQuestionsSuccess = createAction(
    '[AccountQuizesViewPage] Load Questions Success',
    props<{ response: PaginationResponse<Question> }>()
  );

  public static loadQuestionsFailure = createAction(
    '[AccountQuizesViewPage] Load Questions Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static addQuestion = createAction(
    '[AccountQuizesViewPage] Add Question',
    props<{ question: Question }>()
  );

  public static updateQuestion = createAction(
    '[AccountQuizesViewPage] Update Question',
    props<{ updatedQuestion: Question, oldQuestionId: number }>()
  );

  public static deleteQuestion = createAction(
    '[AccountQuizesViewPage] Delete Question',
    props<{ id: number }>()
  );

  public static deleteQuestionSuccess = createAction(
    '[AccountQuizesViewPage] Delete Question Success',
    props<{ id: number }>()
  );

  public static deleteQuestionFailure = createAction(
    '[AccountQuizesViewPage] Delete Question Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
