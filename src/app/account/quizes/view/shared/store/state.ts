import { Quiz } from '@shared/quiz';
import { HttpErrorResponse } from '@angular/common/http';
import { Question } from '@shared/question';

export class AccountQuizesViewPageState {
  public isLoading: boolean;
  public isDeletingQuestion: boolean;
  public quiz: Quiz;
  public questions: Array<Question>;
  public quizErrorResponse: HttpErrorResponse;
  public questionsErrorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.isDeletingQuestion = false;
    this.quiz = null;
    this.questions = [];
    this.quizErrorResponse = null;
    this.questionsErrorResponse = null;
  }
}
