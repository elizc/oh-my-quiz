import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AccountQuizesViewPageState } from './state';
import { Quiz } from '@shared/quiz';
import { Question } from '@shared/question';

const selectFeature = (state: AppState) => state.accountQuizesViewPage;

export class AccountQuizesViewPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AccountQuizesViewPageState) => state.isLoading
  );

  public static isDeletingQuestion: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AccountQuizesViewPageState) => state.isDeletingQuestion
  );

  public static quiz: MemoizedSelector<AppState, Quiz> = createSelector(
    selectFeature,
    (state: AccountQuizesViewPageState) => state.quiz
  );

  public static questions: MemoizedSelector<AppState, Array<Question>> = createSelector(
    selectFeature,
    (state: AccountQuizesViewPageState) => state.questions
  );
}
