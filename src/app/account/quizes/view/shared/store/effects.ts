import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, flatMap, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { AccountQuizesViewPageActions } from './actions';
import { QuizService } from '@shared/quiz';
import { NavigationSelectors } from '@shared/navigation';
import { QuestionFilters, QuestionService } from '@shared/question';
import { Router } from '@angular/router';
import { Game, GameService } from '@shared/game';
import { ModalActions } from '@shared/modal';

@Injectable()
export class AccountQuizesViewPageEffects {
  public initPage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesViewPageActions.initPage),
      withLatestFrom(this.store.select(NavigationSelectors.routeParam, 'id')),
      mergeMap(([_, id]) => [
        AccountQuizesViewPageActions.loadQuiz({ quizId: Number(id) }),
        AccountQuizesViewPageActions.loadQuestions({ quizId: Number(id) })
      ])
    )
  );

  public loadQuiz$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesViewPageActions.loadQuiz),
      switchMap((action) => {
        return this.quizService.loadByID(action.quizId, ['img'])
          .pipe(
            map((quiz) => AccountQuizesViewPageActions.loadQuizSuccess({ quiz })),
            catchError((response) => of(AccountQuizesViewPageActions.loadQuizFailure({ response })))
          );
      })
    )
  );

  public loadQuestions$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesViewPageActions.loadQuestions),
      switchMap((action) => {
        const filters = new QuestionFilters({ quizId: action.quizId });

        return this.questionService.search({ all: true, filters, with: ['img'] })
          .pipe(
            map((response) => AccountQuizesViewPageActions.loadQuestionsSuccess({ response })),
            catchError((response) => of(AccountQuizesViewPageActions.loadQuestionsFailure({ response })))
          );
      })
    )
  );

  public deleteQuestion$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesViewPageActions.deleteQuestion),
      switchMap((action) => {
        return this.questionService.delete(action.id)
          .pipe(
            mergeMap(() => [
              AccountQuizesViewPageActions.deleteQuestionSuccess({ id: action.id }),
              ModalActions.closeAll()
            ]),
            catchError((response) => of(AccountQuizesViewPageActions.deleteQuestionFailure({ response })))
          );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private quizService: QuizService,
    private gameService: GameService,
    private router: Router,
    private questionService: QuestionService
  ) {
  }
}
