import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountQuizesViewPageComponent } from './view.component';
import { AccountQuizesViewPageRoutingModule } from './view.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { accountQuizesViewPageReducer, AccountQuizesViewPageEffects } from './shared/store';
import { QuestionModule } from '@shared/question';
import { QuizModule } from '@shared/quiz';
import { AccountQuizesEditQuestionModalModule } from '@app/account/quizes/shared/modals/edit-question-modal/edit-question-modal.module';
import { AccountQuizesDeleteQuestionModalModule } from '@app/account/quizes/shared/modals/delete-question/delete-question-modal.module';

@NgModule({
  declarations: [
    AccountQuizesViewPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    AccountQuizesViewPageRoutingModule,
    AccountQuizesEditQuestionModalModule,
    AccountQuizesDeleteQuestionModalModule,
    StoreModule.forFeature('accountQuizesViewPage', accountQuizesViewPageReducer),
    EffectsModule.forFeature([AccountQuizesViewPageEffects]),
    QuestionModule,
    QuizModule
  ],
  providers: []
})
export class AccountQuizesViewPageModule { }
