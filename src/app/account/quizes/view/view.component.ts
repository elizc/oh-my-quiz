import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Quiz } from '@shared/quiz';
import { Observable } from 'rxjs';
import { AccountQuizesViewPageActions, AccountQuizesViewPageSelectors } from './shared/store';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { Question } from '@shared/question';
import { ModalActions } from '@shared/modal';
import { AccountQuizesEditModalComponent } from '@app/account/quizes/shared/modals/edit-modal/edit-modal.component';
import { AccountQuizesPageActions, AccountQuizesPageSelectors } from '@app/account/quizes/shared/store';
import { AccountQuizesEditQuestionModalComponent } from '@app/account/quizes/shared/modals/edit-question-modal/edit-question-modal.component';
import { AccountQuizesDeleteQuestionModalComponent } from '@app/account/quizes/shared/modals/delete-question/delete-question-modal.component';

@Component({
  selector: 'account-quizes-view-page',
  templateUrl: 'view.html',
  styleUrls: ['view.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountQuizesViewPageComponent implements OnInit, OnDestroy {
  public quiz$: Observable<Quiz>;
  public questions$: Observable<Array<Question>>;
  public isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.quiz$ = this.store.select(AccountQuizesViewPageSelectors.quiz);
    this.questions$ = this.store.select(AccountQuizesViewPageSelectors.questions);
    this.isLoading$ = this.store.select(AccountQuizesPageSelectors.isLoading);
  }

  public ngOnInit(): void {
    this.store.dispatch(AccountQuizesViewPageActions.initPage());
  }

  public ngOnDestroy(): void {
    this.store.dispatch(AccountQuizesPageActions.resetState());
  }

  public edit(quiz: Quiz): void {
    this.store.dispatch(ModalActions.open({
      component: AccountQuizesEditModalComponent,
      config: {
        data: quiz,
        maxWidth: '360px'
      }
    }));
  }

  public play(quiz: Quiz): void {
    this.store.dispatch(AccountQuizesPageActions.play({quiz}));
  }

  public createQuestion(quiz: Quiz): void {
    this.store.dispatch(ModalActions.open({
      component: AccountQuizesEditQuestionModalComponent,
      config: {
        data: { quiz },
        maxWidth: '360px'
      }
    }));
  }

  public editQuestion(quiz: Quiz, question: Question): void {
    this.store.dispatch(ModalActions.open({
      component: AccountQuizesEditQuestionModalComponent,
      config: {
        data: {
          quiz,
          question
        },
        maxWidth: '360px'
      }
    }));
  }

  public deleteQuestion(id: number): void {
    this.store.dispatch(ModalActions.open({
      component: AccountQuizesDeleteQuestionModalComponent,
      config: {
        data: { id: id }
      }
    }));
  }
}
