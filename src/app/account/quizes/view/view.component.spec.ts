import { AccountQuizesViewPageComponent } from './view.component';
import { AccountQuizesViewPageEffects, accountQuizesViewPageReducer } from './shared/store';
import { AppState } from '@shared/store/state';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { render, RenderResult } from '@testing-library/angular';
import { configuration } from '@configuration';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('AccountQuizesViewPageComponent', () => {
  let component: RenderResult<AccountQuizesViewPageComponent>;
  let componentInstance: AccountQuizesViewPageComponent;
  let store: Store<AppState>;

  const translation = require(`../../../../assets/i18n/${configuration.languages.default}.json`);

  beforeEach(async () => {
    component = await render(AccountQuizesViewPageComponent, {
      imports: [
        TranslateTestingModule.withTranslations(configuration.languages.default, translation),
        StoreModule.forRoot({}, {
          runtimeChecks: {
            strictStateImmutability: true,
            strictActionImmutability: true
          }
        }),
        StoreModule.forFeature('accountQuizesViewPage', accountQuizesViewPageReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([AccountQuizesViewPageEffects])
      ],
      declarations: [
        AccountQuizesViewPageComponent
      ],
      routes: [],
      providers: []
    });

    componentInstance = component.fixture.componentInstance;
    store = TestBed.inject(Store);
  });

  it('should create', async () => {
    expect(component).toBeDefined();
  });
});

