import { ModalActions } from '@shared/modal';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { AccountQuizesEditModalComponent } from '@app/account/quizes/shared/modals/edit-modal/edit-modal.component';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Quiz } from '@app/shared/quiz';
import { Observable } from 'rxjs';
import { AccountQuizesPageActions, AccountQuizesPageSelectors } from '@app/account/quizes/shared/store';
import { Game } from '@shared/game';

@Component({
  selector: 'account-quizes-page',
  templateUrl: 'quizes.html',
  styleUrls: ['quizes.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountQuizesPageComponent implements OnInit, OnDestroy {
  public quizes$: Observable<Array<Quiz>>;
  public games$: Observable<Array<Game>>;

  constructor(private store: Store<AppState>) {
    this.quizes$ = this.store.select(AccountQuizesPageSelectors.quizes);
    this.games$ = this.store.select(AccountQuizesPageSelectors.games);
  }

  public ngOnInit(): void {
    this.store.dispatch(AccountQuizesPageActions.initPage());
  }

  public ngOnDestroy(): void {
    this.store.dispatch(AccountQuizesPageActions.resetState());
  }

  public create(): void {
    this.store.dispatch(ModalActions.open({
      component: AccountQuizesEditModalComponent,
      config: { maxWidth: '360px' }
    }));
  }
}
