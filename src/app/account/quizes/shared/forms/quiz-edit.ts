import { Media } from '@shared/media';

export class AccountQuizesEditModalForm {
  public name: string;
  public questionsCount: number;
  public media: Array<Media>;
  public respSpeed: boolean;
  public showAnswer: boolean;
  public showStatistics: boolean;
}
