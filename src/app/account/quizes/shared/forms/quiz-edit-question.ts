import { Media } from '@shared/media';

export class AccountQuizesEditQuestionModalForm {
  public questionText: string;
  public answerA: string;
  public answerB: string;
  public answerC: string;
  public answerD: string;
  public correctAnswer: string;
  public time: string;
  public media: Array<Media>;
}
