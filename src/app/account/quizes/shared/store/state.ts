import { Quiz } from '@app/shared/quiz';
import { HttpErrorResponse } from '@angular/common/http';
import { Game } from '@shared/game';

export class AccountQuizesPageState {
  public isLoading: boolean;
  public quizes: Array<Quiz>;
  public games: Array<Game>;
  public quizesErrorResponse: HttpErrorResponse;
  public gamesErrorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.quizes = [];
    this.games = [];
    this.quizesErrorResponse = null;
    this.gamesErrorResponse = null;
  }
}
