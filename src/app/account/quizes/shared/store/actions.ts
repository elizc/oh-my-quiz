import { createAction, props } from '@ngrx/store';
import { PaginationResponse } from '@shared/pagination';
import { Quiz } from '@shared/quiz/models/quiz';
import { HttpErrorResponse } from '@angular/common/http';
import { Game } from '@shared/game';

export class AccountQuizesPageActions {
  /* tslint:disable:typedef */
  public static initPage = createAction(
    '[AccountQuizesPage] Init Page'
  );

  public static resetState = createAction(
    '[AccountQuizesPage] Reset State'
  );

  public static loadQuizes = createAction(
    '[AccountQuizesPage] Load Quizes'
  );

  public static loadQuizesSuccess = createAction(
    '[AccountQuizesPage] Load Quizes Success',
    props<{ response: PaginationResponse<Quiz> }>()
  );

  public static loadQuizesFailure = createAction(
    '[AccountQuizesPage] Load Quizes Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static loadGames = createAction(
    '[AccountQuizesPage] Load Games'
  );

  public static loadGamesSuccess = createAction(
    '[AccountQuizesPage] Load Games Success',
    props<{ response: PaginationResponse<Game> }>()
  );

  public static loadGamesFailure = createAction(
    '[AccountQuizesPage] Load Games Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static addQuiz = createAction(
    '[AccountQuizesPage] Add Quiz',
    props<{ quiz: Quiz }>()
  );


  public static play = createAction(
    '[AccountQuizesPage] Play',
    props<{ quiz: Quiz }>()
  );

  public static createGame = createAction(
    '[AccountQuizesPage] Create Game',
    props<{ game: Game, quiz: Quiz }>()
  );

  public static createGameSuccess = createAction(
    '[AccountQuizesPage] Create Game Success',
    props<{ game: Game, quiz: Quiz }>()
  );

  public static createGameFailure = createAction(
    '[AccountQuizesPage] Create Game Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
