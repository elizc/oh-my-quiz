import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AccountQuizesPageActions } from '@app/account/quizes/shared/store/actions';
import { catchError, map, switchMap, tap, mergeMap } from 'rxjs/operators';
import { QuizService } from '@shared/quiz';
import { Game, GameService, GameFilters } from '@shared/game';
import { PresenterActions } from '@shared/presenter';
import { Router } from '@angular/router';

@Injectable()
export class AccountQuizesPageEffects {
  public initPage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.initPage),
      mergeMap(() => [
        AccountQuizesPageActions.loadQuizes(),
        AccountQuizesPageActions.loadGames()
      ])
    )
  );

  public loadQuizes$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.loadQuizes),
      switchMap(() => {
        return this.quizService.search({ all: true, withCount: ['questions'], with: ['img'] })
          .pipe(
            map((response) => AccountQuizesPageActions.loadQuizesSuccess({ response })),
            catchError((response) => of(AccountQuizesPageActions.loadQuizesFailure({ response })))
          );
      })
    )
  );

  public loadGames$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.loadGames),
      switchMap(() => {
        const filters = new GameFilters({ isCompleted: true });

        return this.gameService.search({ all: true, filters, with: ['quiz', 'teams_score'], desc: true })
          .pipe(
            map((response) => AccountQuizesPageActions.loadGamesSuccess({ response })),
            catchError((response) => of(AccountQuizesPageActions.loadGamesFailure({ response })))
          );
      })
    )
  );

  public play$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.play),
      map((action) => {
        const game = new Game({ ...action.quiz, quizId: action.quiz.id });

        return AccountQuizesPageActions.createGame({ game, quiz: action.quiz });
      })
    )
  );

  public createGame$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.createGame),
      switchMap((action) => {
        return this.gameService.create(action.game)
          .pipe(
            map((game) => AccountQuizesPageActions.createGameSuccess({ game, quiz: action.quiz })),
            catchError((response) => of(AccountQuizesPageActions.createGameFailure({ response })))
          );
      })
    )
  );

  public createGameSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesPageActions.createGameSuccess),
      map((action) => {
        const game = new Game({ ...action.game, quiz: action.quiz });

        return PresenterActions.setGame({ game });
      }),
      tap(() => this.router.navigate(['/presenter/game']))
    )
  );
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private quizService: QuizService,
    private gameService: GameService
  ) { }
}
