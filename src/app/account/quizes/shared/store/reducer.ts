import { Action, createReducer, on } from '@ngrx/store';
import { AccountQuizesPageActions } from './actions';
import { AccountQuizesPageState } from './state';

const initialState = new AccountQuizesPageState();

const reducer = createReducer(
  initialState,
  on(AccountQuizesPageActions.resetState, () => initialState),
  on(AccountQuizesPageActions.loadQuizesSuccess, (state, action) => ({
    ...state,
    quizes: action.response.data
  })),
  on(AccountQuizesPageActions.loadQuizesFailure, (state, action) => ({
    ...state,
    quizesErrorResponse: action.response
  })),
  on(AccountQuizesPageActions.loadGamesSuccess, (state, action) => ({
    ...state,
    games: action.response.data
  })),
  on(AccountQuizesPageActions.loadQuizesFailure, (state, action) => ({
    ...state,
    gamesErrorResponse: action.response
  })),
  on(AccountQuizesPageActions.addQuiz, (state, action) => ({
    ...state,
    quizes: [...state.quizes, action.quiz]
  })),
  on(AccountQuizesPageActions.play, (state) => ({
    ...state,
    isLoading: true
  })),
  on(AccountQuizesPageActions.createGameSuccess, (state, action) => ({
    ...state,
    isLoading: false
  }))
);

export function accountQuizesPageReducer(state: AccountQuizesPageState | undefined, action: Action): AccountQuizesPageState {
  return reducer(state, action);
}
