import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AccountQuizesPageState } from './state';
import { Quiz } from '@app/shared/quiz';
import { Game } from '@shared/game';

const selectFeature = (state: AppState) => state.accountQuizesPage;

export class AccountQuizesPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AccountQuizesPageState) => state.isLoading
  );

  public static quizes: MemoizedSelector<AppState, Array<Quiz>> = createSelector(
    selectFeature,
    (state: AccountQuizesPageState) => state.quizes
  );

  public static games: MemoizedSelector<AppState, Array<Game>> = createSelector(
    selectFeature,
    (state: AccountQuizesPageState) => state.games
  );
}
