import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AccountQuizesDeleteQuestionModalComponent } from '@app/account/quizes/shared/modals/delete-question/delete-question-modal.component';

@NgModule({
  declarations: [
    AccountQuizesDeleteQuestionModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule
  ],
  providers: []
})
export class AccountQuizesDeleteQuestionModalModule { }
