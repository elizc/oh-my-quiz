import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { AccountQuizesViewPageActions, AccountQuizesViewPageSelectors } from '@app/account/quizes/view/shared/store';
import { BaseModalComponent } from '@shared/modal/base-modal-component';

@Component({
  selector: 'account-quizes-delete-question-modal',
  templateUrl: 'delete-question-modal.html',
  styleUrls: ['delete-question-modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountQuizesDeleteQuestionModalComponent extends BaseModalComponent<AccountQuizesDeleteQuestionModalComponent> {
  public isDeleting$: Observable<boolean>;

  constructor(
    dialogRef: MatDialogRef<AccountQuizesDeleteQuestionModalComponent>,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public data?: { id: number }
  ) {
    super(dialogRef);
    this.isDeleting$ = this.store.select(AccountQuizesViewPageSelectors.isDeletingQuestion);
  }

  public remove(): void {
    this.store.dispatch(AccountQuizesViewPageActions.deleteQuestion({ id: this.data.id }));
  }
}
