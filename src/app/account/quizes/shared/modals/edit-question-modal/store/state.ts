import { Question } from '@shared/question';
import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { AccountQuizesEditQuestionModalForm } from '@app/account/quizes/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Media } from '@shared/media';

export class AccountQuizesEditQuestionModalState {
  public isLoading: boolean;
  public question: Question;
  public image: Media;
  public formState: FormGroupState<AccountQuizesEditQuestionModalForm>;
  public errorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.question = null;
    this.formState = createFormGroupState<AccountQuizesEditQuestionModalForm>('AccountQuizesEditQuestionModalForm', {
      questionText: '',
      answerA: '',
      answerB: '',
      answerC: '',
      answerD: '',
      correctAnswer: '',
      time: '5',
      media: []
    });
    this.errorResponse = null;
  }
}
