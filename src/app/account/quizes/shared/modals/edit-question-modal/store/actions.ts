import { createAction, props } from '@ngrx/store';
import { Question } from '@shared/question';
import { HttpErrorResponse } from '@angular/common/http';
import { Media } from '@shared/media';

export class AccountQuizesEditQuestionModalActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[AccountQuizesEditQuestionModal] Reset State'
  );

  public static initModal = createAction(
    '[AccountQuizesEditQuestionModal] Init Modal',
    props<{ question?: Question }>()
  );

  public static prefillFormState = createAction(
    '[AccountQuizesEditQuestionModal] Prefill Form State',
    props<{ question?: Question }>()
  );

  public static submitForm = createAction(
    '[AccountQuizesEditQuestionModal] Submit Form',
    props<{ quizId: number }>()
  );

  public static create = createAction(
    '[AccountQuizesEditQuestionModal] Create',
    props<{ question: Question }>()
  );

  public static createSuccess = createAction(
    '[AccountQuizesEditQuestionModal] Create Success',
    props<{ question: Question }>()
  );

  public static createFailure = createAction(
    '[AccountQuizesEditQuestionModal] Create Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static update = createAction(
    '[AccountQuizesEditQuestionModal] Update',
    props<{ question: Question }>()
  );

  public static updateSuccess = createAction(
    '[AccountQuizesEditQuestionModal] Update Success',
    props<{ updatedQuestion: Question }>()
  );

  public static updateFailure = createAction(
    '[AccountQuizesEditMQuestionModal] Update Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static uploadImage = createAction(
    '[AccountQuizesEditQuestionModal] Upload Image',
    props<{ file: File }>()
  );

  public static uploadImageSuccess = createAction(
    '[AccountQuizesEditQuestionModal] Upload Image Success',
    props<{ media: Media }>()
  );

  public static uploadImageFailure = createAction(
    '[AccountQuizesEditQuestionModal] Upload Image Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
