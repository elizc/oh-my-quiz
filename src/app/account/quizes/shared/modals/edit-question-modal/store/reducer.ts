import { AccountQuizesEditQuestionModalState } from './state';
import { Action, createReducer, on } from '@ngrx/store';
import { AccountQuizesEditQuestionModalActions } from './actions';
import { onNgrxForms, setValue, updateGroup, validate, wrapReducerWithFormStateUpdate } from 'ngrx-forms';
import { AccountQuizesEditQuestionModalForm } from '@app/account/quizes/shared/forms';
import { Media } from '@shared/media';
import { required } from 'ngrx-forms/validation';
import { omit } from 'lodash';

const initialState = new AccountQuizesEditQuestionModalState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(AccountQuizesEditQuestionModalActions.resetState, () => initialState),
  on(AccountQuizesEditQuestionModalActions.initModal, (state, action) => ({
    ...state,
    question: action.question
  })),
  on(AccountQuizesEditQuestionModalActions.prefillFormState, (state, action) => ({
    ...state,
    formState: updateGroup<AccountQuizesEditQuestionModalForm>(
      state.formState,
      {
        questionText: setValue(action.question?.text),
        answerA: setValue(action.question?.answerA),
        answerB: setValue(action.question?.answerB),
        answerC: setValue(action.question?.answerC),
        answerD: setValue(action.question?.answerD),
        correctAnswer: setValue(action.question?.correctAnswer),
        time: setValue(action.question?.time.toString()),
        media: setValue(action.question  && action.question.image ? [new Media(action.question.image)] : [])
      }
    )
  })),
  on(AccountQuizesEditQuestionModalActions.create, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(AccountQuizesEditQuestionModalActions.createSuccess, (state) => ({
    ...state,
    isLoading: false
  })),
  on(AccountQuizesEditQuestionModalActions.createFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  })),
  on(AccountQuizesEditQuestionModalActions.update, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(AccountQuizesEditQuestionModalActions.updateSuccess, (state) => ({
    ...state,
    isLoading: false,
    errorResponse: null
  })),
  on(AccountQuizesEditQuestionModalActions.updateFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  })),
  on(AccountQuizesEditQuestionModalActions.uploadImageSuccess, (state, action) => ({
    ...state,
    image: action.media,
    formState: updateGroup<AccountQuizesEditQuestionModalForm>(
      state.formState,
      { media: setValue([new Media(omit(action.media, ['createdAt', 'updatedAt', 'deletedAt']))])  }
    )
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  updateGroup<AccountQuizesEditQuestionModalForm>({
    questionText: validate(required),
    answerA: validate(required),
    answerB: validate(required),
    answerC: validate(required),
    answerD: validate(required),
    correctAnswer: validate(required),
    time: validate(required)
  })
);

export function accountQuizesEditQuestionModalReducer (
  state: AccountQuizesEditQuestionModalState | undefined,
  action: Action
): AccountQuizesEditQuestionModalState {
  return reducer(state, action);
}
