import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { MediaService } from '@shared/media';
import { Question, QuestionService } from '@shared/question';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { AccountQuizesEditQuestionModalActions } from '@app/account/quizes/shared/modals/edit-question-modal/store/actions';
import { AccountQuizesEditQuestionModalSelectors } from '@app/account/quizes/shared/modals/edit-question-modal/store/selectors';
import { ModalActions } from '@shared/modal';
import { notificationActions } from '@shared/notification';
import { AccountQuizesViewPageActions } from '@app/account/quizes/view/shared/store';

@Injectable()
export class AccountQuizesEditQuestionModalEffects {
  public initModal$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.initModal),
      map((action) => AccountQuizesEditQuestionModalActions.prefillFormState({
        question: action.question
      }))
    )
  );

  public uploadImage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.uploadImage),
      switchMap((action) => {
        return this.mediaService.create(action.file)
          .pipe(
            switchMap((media) => of(
              AccountQuizesEditQuestionModalActions.uploadImageSuccess({ media })
            )),
            catchError((response) => of(
              AccountQuizesEditQuestionModalActions.uploadImageFailure({ response })
            ))
          );
      })
    )
  );

  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.submitForm),
      withLatestFrom(
        this.store.select(AccountQuizesEditQuestionModalSelectors.formState),
        this.store.select(AccountQuizesEditQuestionModalSelectors.question)
      ),
      filter(([_, formState]) => formState.isValid),
      map(([action, formState, questionFromState]) => {
        const question = new Question({
          id: (!!questionFromState) ? questionFromState.id : null,
          text: formState.value.questionText,
          answerA: formState.value.answerA,
          answerB: formState.value.answerB,
          answerC: formState.value.answerC,
          answerD: formState.value.answerD,
          correctAnswer: formState.value.correctAnswer,
          time: Number(formState.value.time),
          imageID: (!!formState.value.media[0]) ? formState.value.media[0].id : null,
          quizId: action.quizId
        });

        return (!!questionFromState)
          ? AccountQuizesEditQuestionModalActions.update({ question })
          : AccountQuizesEditQuestionModalActions.create({ question });
      })
    )
  );

  public create$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.create),
      mergeMap((action) => {
        return this.questionService
          .create(action.question)
          .pipe(
            map((question) => AccountQuizesEditQuestionModalActions.createSuccess({ question })),
            catchError((response) => of(AccountQuizesEditQuestionModalActions.createFailure({ response })))
          );
      })
    )
  );

  public createSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.createSuccess),
      withLatestFrom(this.store.select(AccountQuizesEditQuestionModalSelectors.image)),
      map(([action, image]) => new Question({ ...action.question, image })),
      mergeMap((question) => [
        ModalActions.closeAll(),
        notificationActions.showSuccess({
          translationKey: 'ACCOUNT.QUIZES.MODAL.EDIT_QUESTION.TEXT_QUESTION_CREATED_SUCCESS'
        }),
        AccountQuizesViewPageActions.addQuestion({ question: question })
      ])
    )
  );

  public update$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.update),
      mergeMap((action) => {
        return this.questionService
          .update(action.question)
          .pipe(
            map((question) => AccountQuizesEditQuestionModalActions.updateSuccess({
              updatedQuestion: question
            })),
            catchError((response) => of(AccountQuizesEditQuestionModalActions.updateFailure({ response })))
          );
      })
    )
  );

  public updateSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditQuestionModalActions.updateSuccess),
      withLatestFrom(this.store.select(AccountQuizesEditQuestionModalSelectors.image)),
      map(([action, image]) => new Question({ ...action.updatedQuestion, image })),
      withLatestFrom(this.store.select(AccountQuizesEditQuestionModalSelectors.question)),
      mergeMap(([updatedQuestion, oldQuestion]) => [
        ModalActions.closeAll(),
        notificationActions.showSuccess({
          translationKey: 'ACCOUNT.QUIZES.MODAL.EDIT_QUESTION.TEXT_QUESTION_UPDATED_SUCCESS'
        }),
        AccountQuizesViewPageActions.updateQuestion({
          updatedQuestion: updatedQuestion,
          oldQuestionId: oldQuestion.id
        })
      ])
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private mediaService: MediaService,
    private questionService: QuestionService
  ) { }
}
