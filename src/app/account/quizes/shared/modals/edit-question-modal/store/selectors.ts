import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { Question } from '@shared/question';
import { AccountQuizesEditQuestionModalState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { AccountQuizesEditQuestionModalForm } from '@app/account/quizes/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Media } from '@shared/media';

const selectFeature = (state: AppState) => state.accountQuizesEditQuestionModal;

export class AccountQuizesEditQuestionModalSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AccountQuizesEditQuestionModalState) => state.isLoading
  );

  public static question: MemoizedSelector<AppState, Question> = createSelector(
    selectFeature,
    (state: AccountQuizesEditQuestionModalState) => state.question
  );

  public static image: MemoizedSelector<AppState, Media> = createSelector(
    selectFeature,
    (state: AccountQuizesEditQuestionModalState) => state.image
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<AccountQuizesEditQuestionModalForm>> = createSelector(
    selectFeature,
    (state: AccountQuizesEditQuestionModalState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: AccountQuizesEditQuestionModalState) => state.errorResponse
  );
}
