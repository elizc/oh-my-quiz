import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { BaseModalComponent } from '@shared/modal/base-modal-component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { Question } from '@shared/question';
import { AccountQuizesEditQuestionModalActions } from '@app/account/quizes/shared/modals/edit-question-modal/store/actions';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { AccountQuizesEditQuestionModalForm } from '@app/account/quizes/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { AccountQuizesEditQuestionModalSelectors } from '@app/account/quizes/shared/modals/edit-question-modal/store';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'account-quizes-edit-question-modal',
  templateUrl: 'edit-question-modal.html',
  styleUrls: ['edit-question-modal.scss']
})
export class AccountQuizesEditQuestionModalComponent
  extends BaseModalComponent<AccountQuizesEditQuestionModalComponent>
  implements OnInit, OnDestroy {

  public formState$: Observable<FormGroupState<AccountQuizesEditQuestionModalForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;
  public question: Question;
  public quiz: Quiz;

  public get isEdit(): boolean {
    return !!this.question;
  }

  constructor(
    dialogRef: MatDialogRef<AccountQuizesEditQuestionModalComponent>,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public data: { quiz: Quiz, question: Question }
  ) {
    super(dialogRef);
    this.formState$ = this.store.select(AccountQuizesEditQuestionModalSelectors.formState);
    this.isLoading$ = this.store.select(AccountQuizesEditQuestionModalSelectors.isLoading);
    this.errorResponse$ = this.store.select(AccountQuizesEditQuestionModalSelectors.errorResponse);
    this.question = this.data.question;
    this.quiz = this.data.quiz;
  }

  public ngOnInit(): void {
    this.store.dispatch(AccountQuizesEditQuestionModalActions.initModal({ question: this.question }));
  }

  public ngOnDestroy(): void {
    this.store.dispatch(AccountQuizesEditQuestionModalActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(AccountQuizesEditQuestionModalActions.submitForm({ quizId: this.quiz.id }));
  }

  public onUploadFile(file: FileList): void {
    this.store.dispatch(AccountQuizesEditQuestionModalActions.uploadImage({ file: file[0] }));
  }
}
