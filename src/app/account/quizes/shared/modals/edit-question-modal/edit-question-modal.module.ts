import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgrxFormsModule } from 'ngrx-forms';
import { AccountQuizesEditQuestionModalComponent } from '@app/account/quizes/shared/modals/edit-question-modal/edit-question-modal.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  AccountQuizesEditQuestionModalEffects,
  accountQuizesEditQuestionModalReducer
} from '@app/account/quizes/shared/modals/edit-question-modal/store';

@NgModule({
  declarations: [
    AccountQuizesEditQuestionModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgrxFormsModule,
    StoreModule.forFeature('accountQuizesEditQuestionModal', accountQuizesEditQuestionModalReducer),
    EffectsModule.forFeature([AccountQuizesEditQuestionModalEffects])
  ],
  providers: []
})
export class AccountQuizesEditQuestionModalModule { }
