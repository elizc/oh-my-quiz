import { Action, createReducer, on } from '@ngrx/store';
import {AccountQuizesEditModalActions } from './actions';
import { AccountQuizesEditModalState } from './state';
import { onNgrxForms, setValue, updateGroup, validate, wrapReducerWithFormStateUpdate } from 'ngrx-forms';
import { greaterThanOrEqualTo, required } from 'ngrx-forms/validation';
import { AccountQuizesEditModalForm } from '@app/account/quizes/shared/forms';
import { Media } from '@shared/media';
import { omit } from 'lodash';

const initialState = new AccountQuizesEditModalState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(AccountQuizesEditModalActions.resetState, () => initialState),
  on(AccountQuizesEditModalActions.create, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(AccountQuizesEditModalActions.createSuccess, (state) => ({
    ...state,
    isLoading: false
  })),
  on(AccountQuizesEditModalActions.createFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  })),
  on(AccountQuizesEditModalActions.update, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(AccountQuizesEditModalActions.updateSuccess, (state) => ({
    ...state,
    isLoading: false,
    errorResponse: null
  })),
  on(AccountQuizesEditModalActions.updateFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  })),
  on(AccountQuizesEditModalActions.uploadImage, (state) => ({
    ...state,
    isLoading: true
  })),
  on(AccountQuizesEditModalActions.uploadImageSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    formState: updateGroup<AccountQuizesEditModalForm>(
      state.formState,
      { media: setValue([new Media(omit(action.media, ['createdAt', 'updatedAt', 'deletedAt']))]) }
    )
  })),
  on(AccountQuizesEditModalActions.initModal, (state, action) => ({
    ...state,
    quiz: action.quiz
  })),
  on(AccountQuizesEditModalActions.prefillFormState, (state, action) => ({
    ...state,
    formState: updateGroup<AccountQuizesEditModalForm>(
      state.formState,
      {
        name: setValue(action.quiz?.name),
        questionsCount: setValue(action.quiz?.capacity),
        media: (!!action.quiz?.image) ? setValue([action.quiz.image]) : setValue([]),
        respSpeed: setValue(action.quiz?.isConsiderResponseSpeed),
        showAnswer: setValue(action.quiz?.isShowAnswer),
        showStatistics: setValue(action.quiz?.isShowStatistics)
      }
    )
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  updateGroup<AccountQuizesEditModalForm>({
    name: validate(required),
    questionsCount: validate([
      required,
      greaterThanOrEqualTo(1)
    ]),
    respSpeed: validate(required),
    showAnswer: validate(required),
    showStatistics: validate(required)
  })
);

export function accountQuizesEditModalReducer(state: AccountQuizesEditModalState | undefined, action: Action): AccountQuizesEditModalState {
  return reducer(state, action);
}
