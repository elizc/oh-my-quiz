import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Media } from '@shared/media';
import { Quiz } from '@shared/quiz';

export class AccountQuizesEditModalActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[AccountQuizesEditModal] Reset State'
  );

  public static initModal = createAction(
    '[AccountQuizesEditModal] Init Modal',
    props<{ quiz?: Quiz }>()
  );

  public static prefillFormState = createAction(
    '[AccountQuizesEditModal] Prefill Form State',
    props<{ quiz?: Quiz }>()
  );

  public static submitForm = createAction(
    '[AccountQuizesEditModal] Submit Form'
  );

  public static create = createAction(
    '[AccountQuizesEditModal] Create',
    props<{ quiz: Quiz }>()
  );

  public static createSuccess = createAction(
    '[AccountQuizesEditModal] Create Success',
    props<{ quiz: Quiz }>()
  );

  public static createFailure = createAction(
    '[AccountQuizesEditModal] Create Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static update = createAction(
    '[AccountQuizesEditModal] Update',
    props<{ quiz: Quiz }>()
  );

  public static updateSuccess = createAction(
    '[AccountQuizesEditModal] Update Success',
    props<{ quiz: Quiz }>()
  );

  public static updateFailure = createAction(
    '[AccountQuizesEditModal] Update Failure',
    props<{ response: HttpErrorResponse }>()
  );

  public static uploadImage = createAction(
    '[AccountQuizesEditModal] Upload Image',
    props<{ file: File }>()
  );

  public static uploadImageSuccess = createAction(
    '[AccountQuizesEditModal] Upload Image Success',
    props<{ media: Media }>()
  );

  public static uploadImageFailure = createAction(
    '[AccountQuizesEditModal] Upload Image Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
