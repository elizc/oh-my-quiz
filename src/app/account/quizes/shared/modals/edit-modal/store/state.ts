import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { AccountQuizesEditModalForm } from '@app/account/quizes/shared/forms';
import { Quiz } from '@shared/quiz';

export class AccountQuizesEditModalState {
  public isLoading: boolean;
  public formState: FormGroupState<AccountQuizesEditModalForm>;
  public errorResponse: HttpErrorResponse;
  public quiz: Quiz;

  constructor() {
    this.isLoading = false;
    this.formState = createFormGroupState<AccountQuizesEditModalForm>('AccountQuizesEditModalForm', {
      name: '',
      questionsCount: null,
      media: [],
      respSpeed: false,
      showAnswer: false,
      showStatistics: false
    });
    this.errorResponse = null;
    this.quiz = null;
  }
}
