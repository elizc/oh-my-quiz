import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { AccountQuizesEditModalActions } from '@app/account/quizes/shared/modals/edit-modal/store/actions';
import { MediaService } from '@shared/media';
import { Quiz, QuizService } from '@shared/quiz';
import { AccountQuizesEditModalSelectors } from '@app/account/quizes/shared/modals/edit-modal/store/selectors';
import { notificationActions } from '@shared/notification';
import { ModalActions } from '@shared/modal';
import { AccountQuizesPageActions } from '@app/account/quizes/shared/store';
import { AccountQuizesViewPageActions } from '@app/account/quizes/view/shared/store';

@Injectable()
export class AccountQuizesEditModalEffects {
  public initModal$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.initModal),
      map((action) => AccountQuizesEditModalActions.prefillFormState({ quiz: action.quiz }))
    )
  );

  public uploadImage$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.uploadImage),
      switchMap((action) => {
        return this.mediaService.create(action.file)
          .pipe(
            switchMap((media) => of(AccountQuizesEditModalActions.uploadImageSuccess({ media }))),
            catchError((response) => of(AccountQuizesEditModalActions.uploadImageFailure({ response })))
          );
      })
    )
  );

  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.submitForm),
      withLatestFrom(
        this.store.select(AccountQuizesEditModalSelectors.formState),
        this.store.select(AccountQuizesEditModalSelectors.quiz)
      ),
      filter(([_, formState]) => formState.isValid),
      map(([_, formState, quizFromState]) => {
        const quiz = new Quiz({
          id: (!!quizFromState) ? quizFromState.id : null,
          name: formState.value.name,
          capacity: formState.value.questionsCount,
          imageID: (!!formState.value.media[0]) ? formState.value.media[0].id : null,
          isConsiderResponseSpeed: formState.value.respSpeed
          // isShowStatistics: formState.value.showAnswer,
          // isShowAnswer: formState.value.showStatistics
        });

        return (!!quizFromState)
          ? AccountQuizesEditModalActions.update({ quiz })
          : AccountQuizesEditModalActions.create({ quiz });
      })
    )
  );

  public create$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.create),
      mergeMap((action) => {
        return this.quizService
          .create(action.quiz)
          .pipe(
            map((quiz) => AccountQuizesEditModalActions.createSuccess({ quiz })),
            catchError((response) => of(AccountQuizesEditModalActions.createFailure({ response })))
          );
      })
    )
  );

  public createSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.createSuccess),
      mergeMap((action) => [
        ModalActions.closeAll(),
        notificationActions.showSuccess({ translationKey: 'ACCOUNT.QUIZES.MODAL.EDIT.TEXT_QUIZ_CREATED_SUCCESS' }),
        AccountQuizesPageActions.addQuiz({ quiz: new Quiz({ ...action.quiz, questionsCount: 0 }) })
      ])
    )
  );

  public update$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.update),
      mergeMap((action) => {
        return this.quizService
          .update(action.quiz)
          .pipe(
            map((quiz) => AccountQuizesEditModalActions.updateSuccess({ quiz })),
            catchError((response) => of(AccountQuizesEditModalActions.updateFailure({ response })))
          );
      })
    )
  );

  public updateSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(AccountQuizesEditModalActions.updateSuccess),
      mergeMap(() => [
        ModalActions.closeAll(),
        AccountQuizesViewPageActions.initPage(),
        notificationActions.showSuccess({ translationKey: 'ACCOUNT.QUIZES.MODAL.EDIT.TEXT_QUIZ_UPDATED_SUCCESS' })
      ])
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private mediaService: MediaService,
    private quizService: QuizService
  ) { }
}
