import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AccountQuizesEditModalState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { AccountQuizesEditModalForm } from '@app/account/quizes/shared/forms';
import { Quiz } from '@shared/quiz';

const selectFeature = (state: AppState) => state.accountQuizesEditModal;

export class AccountQuizesEditModalSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: AccountQuizesEditModalState) => state.isLoading
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<AccountQuizesEditModalForm>> = createSelector(
    selectFeature,
    (state: AccountQuizesEditModalState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: AccountQuizesEditModalState) => state.errorResponse
  );

  public static quiz: MemoizedSelector<AppState, Quiz> = createSelector(
    selectFeature,
    (state: AccountQuizesEditModalState) => state.quiz
  );
}
