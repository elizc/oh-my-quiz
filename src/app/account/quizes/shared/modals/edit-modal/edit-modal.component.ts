import { Component, ChangeDetectionStrategy, OnDestroy, Inject, OnInit } from '@angular/core';
import { BaseModalComponent } from '../../../../../shared/modal/base-modal-component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { AccountQuizesEditModalForm } from '@app/account/quizes/shared/forms';
import {
  AccountQuizesEditModalActions,
  AccountQuizesEditModalSelectors
} from '@app/account/quizes/shared/modals/edit-modal/store';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'account-quizes-edit-modal',
  templateUrl: 'edit-modal.html',
  styleUrls: ['edit-modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountQuizesEditModalComponent extends BaseModalComponent<AccountQuizesEditModalComponent> implements OnInit, OnDestroy {
  public formState$: Observable<FormGroupState<AccountQuizesEditModalForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;

  public get isEdit(): boolean {
    return !!this.quiz;
  }

  constructor(
    dialogRef: MatDialogRef<AccountQuizesEditModalComponent>,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public quiz?: Quiz
  ) {
    super(dialogRef);
    this.formState$ = this.store.select(AccountQuizesEditModalSelectors.formState);
    this.isLoading$ = this.store.select(AccountQuizesEditModalSelectors.isLoading);
    this.errorResponse$ = this.store.select(AccountQuizesEditModalSelectors.errorResponse);
  }

  public ngOnInit(): void {
    this.store.dispatch(AccountQuizesEditModalActions.initModal({ quiz: this.quiz }));
  }

  public ngOnDestroy(): void {
    this.store.dispatch(AccountQuizesEditModalActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(AccountQuizesEditModalActions.submitForm());
  }

  public onUploadFile(file: FileList): void {
    this.store.dispatch(AccountQuizesEditModalActions.uploadImage({ file: file[0] }));
  }
}
