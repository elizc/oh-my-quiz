import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  AccountQuizesEditModalEffects,
  accountQuizesEditModalReducer
} from '@app/account/quizes/shared/modals/edit-modal/store';
import { NgrxFormsModule } from 'ngrx-forms';
import { AccountQuizesEditModalComponent } from '@app/account/quizes/shared/modals/edit-modal/edit-modal.component';

@NgModule({
  declarations: [
    AccountQuizesEditModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgrxFormsModule,
    StoreModule.forFeature('accountQuizesEditModal', accountQuizesEditModalReducer),
    EffectsModule.forFeature([AccountQuizesEditModalEffects])
  ],
  providers: []
})
export class AccountQuizesEditModalModule { }
