import { Component, Input } from '@angular/core';
import { Quiz } from '@shared/quiz';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { AccountQuizesPageActions, AccountQuizesPageSelectors } from '@app/account/quizes/shared/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'quiz-item',
  templateUrl: 'quiz-item.html',
  styleUrls: ['quiz-item.scss']
})
export class QuizItemComponent {
  @Input() quiz: Quiz;
  public isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.isLoading$ = this.store.select(AccountQuizesPageSelectors.isLoading);
  }

  public play(): void {
    this.store.dispatch(AccountQuizesPageActions.play({ quiz: this.quiz }));
  }
}
