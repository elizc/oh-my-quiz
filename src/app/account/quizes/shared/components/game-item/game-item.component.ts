import { Component, Input } from '@angular/core';
import { Game } from '@shared/game';

@Component({
  selector: 'game-item',
  templateUrl: 'game-item.html',
  styleUrls: ['game-item.scss']
})
export class GameItemComponent {
  @Input() game: Game;
}
