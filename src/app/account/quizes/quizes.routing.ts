import { AccountQuizesPageComponent } from './quizes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AccountQuizesPageComponent
  },
  {
    path: ':id',
    loadChildren: () => import('./view/view.module').then((module) => module.AccountQuizesViewPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountQuizesPageRoutingModule { }
