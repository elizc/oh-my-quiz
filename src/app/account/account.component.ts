import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { User, UserSelectors } from '@shared/user';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { authActions } from '@shared/auth';

@Component({
  selector: 'app-account-root',
  templateUrl: 'account.html',
  styleUrls: ['account.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountComponent {
  public user$: Observable<User>;

  constructor(private store: Store<AppState>) {
    this.user$ = this.store.select(UserSelectors.profile);
  }

  public logout(): void {
    this.store.dispatch(authActions.unauthorize());
  }
}
