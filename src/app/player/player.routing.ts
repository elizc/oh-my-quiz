import { PlayerComponent } from './player.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HasPlayerTeamGuard } from '@shared/player';

const routes: Routes = [
  {
    path: '',
    component: PlayerComponent,
    children: [
      {
        path: 'join',
        loadChildren: () => import('./join/join.module').then((module) => module.PlayerJoinPageModule)
      },
      {
        path: 'game',
        loadChildren: () => import('./game/game.module').then((module) => module.PlayerGamePageModule),
        canActivate: [HasPlayerTeamGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }
