import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { PlayerJoinPageForm } from '@app/player/join/shared/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerJoinPageActions, PlayerJoinPageSelectors } from '@app/player/join/shared/store';
import { PlayerActions, PlayerSelectors } from '@shared/player';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'player-join-page',
  templateUrl: 'join.html',
  styleUrls: ['join.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerJoinPageComponent implements OnInit, OnDestroy {
  public formState$: Observable<FormGroupState<PlayerJoinPageForm>>;
  public isLoading$: Observable<boolean>;
  public errorResponse$: Observable<HttpErrorResponse>;
  public quiz$: Observable<Quiz>;

  constructor(private store: Store<AppState>) {
    this.formState$ = this.store.select(PlayerJoinPageSelectors.formState);
    this.isLoading$ = this.store.select(PlayerJoinPageSelectors.isLoading);
    this.errorResponse$ = this.store.select(PlayerJoinPageSelectors.errorResponse);
    this.quiz$ = this.store.select(PlayerSelectors.quiz);
  }

  public ngOnInit(): void {
    this.store.dispatch(PlayerActions.init());
  }

  public ngOnDestroy(): void {
    this.store.dispatch(PlayerJoinPageActions.resetState());
  }

  public onSubmit(): void {
    this.store.dispatch(PlayerJoinPageActions.submitForm());
  }

  public addPlayer(): void {
    this.store.dispatch(PlayerJoinPageActions.addPlayer());
  }

  public trackByIndex(index: number): number {
    return index;
  }
}
