import { Action, createReducer, on } from '@ngrx/store';
import { PlayerJoinPageActions } from './actions';
import { PlayerJoinPageState } from './state';
import { onNgrxForms, setValue, updateGroup, validate, wrapReducerWithFormStateUpdate } from 'ngrx-forms';
import { PlayerJoinPageForm } from '@app/player/join/shared/forms';
import { required } from 'ngrx-forms/validation';
import { playersValidator } from '@shared/players-validator';

const initialState = new PlayerJoinPageState();

const pageReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(PlayerJoinPageActions.resetState, () => initialState),
  on(PlayerJoinPageActions.addPlayer, (state) => ({
    ...state,
    formState: updateGroup<PlayerJoinPageForm>(
      state.formState,
      {
        players: setValue([...state.formState.value.players, ''])
      }
    )
  })),
  on(PlayerJoinPageActions.create, (state) => ({
    ...state,
    isLoading: true,
    errorResponse: null
  })),
  on(PlayerJoinPageActions.createSuccess, (state) => ({
    ...state,
    isLoading: false
  })),
  on(PlayerJoinPageActions.createFailure, (state, action) => ({
    ...state,
    isLoading: false,
    errorResponse: action.response
  }))
);

const reducer = wrapReducerWithFormStateUpdate(
  pageReducer,
  (state) => state.formState,
  (formState) => updateGroup<PlayerJoinPageForm>(formState, {
    teamName: validate(required),
    players: validate(playersValidator(formState.controls.players))
  })
);

export function playerJoinPageReducer(state: PlayerJoinPageState | undefined, action: Action): PlayerJoinPageState {
  return reducer(state, action);
}
