import { createFormGroupState, FormGroupState } from 'ngrx-forms';
import { HttpErrorResponse } from '@angular/common/http';
import { PlayerJoinPageForm } from '@app/player/join/shared/forms';

export class PlayerJoinPageState {
  public isLoading: boolean;
  public formState: FormGroupState<PlayerJoinPageForm>;
  public errorResponse: HttpErrorResponse;

  constructor() {
    this.isLoading = false;
    this.formState = createFormGroupState<PlayerJoinPageForm>('PlayerJoinPageForm', {
      teamName: '',
      players: ['', '']
    });
    this.errorResponse = null;
  }
}
