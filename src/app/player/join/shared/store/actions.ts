import { createAction, props } from '@ngrx/store';
import { Team } from '@shared/team';
import { HttpErrorResponse } from '@angular/common/http';

export class PlayerJoinPageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PlayerJoinPage] Reset State'
  );

  public static addPlayer = createAction(
    '[PlayerJoinPage] Add Player'
  );

  public static submitForm = createAction(
    '[PlayerJoinPage] Submit Form'
  );

  public static create = createAction(
    '[PlayerJoinPage] Create',
    props<{ team: Team }>()
  );

  public static createSuccess = createAction(
    '[PlayerJoinPage] Create Success',
    props<{ team: Team}>()
  );

  public static createFailure = createAction(
    '[PlayerJoinPage] Create Failure',
    props<{ response: HttpErrorResponse }>()
  );
  /* tslint:enable:typedef */
}
