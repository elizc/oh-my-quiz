import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PlayerJoinPageState } from './state';
import { FormGroupState } from 'ngrx-forms';
import { PlayerJoinPageForm } from '@app/player/join/shared/forms';
import { HttpErrorResponse } from '@angular/common/http';

const selectFeature = (state: AppState) => state.playerJoinPage;

export class PlayerJoinPageSelectors {
  public static isLoading: MemoizedSelector<AppState, boolean> = createSelector(
    selectFeature,
    (state: PlayerJoinPageState) => state.isLoading
  );

  public static formState: MemoizedSelector<AppState, FormGroupState<PlayerJoinPageForm>> = createSelector(
    selectFeature,
    (state: PlayerJoinPageState) => state.formState
  );

  public static errorResponse: MemoizedSelector<AppState, HttpErrorResponse> = createSelector(
    selectFeature,
    (state: PlayerJoinPageState) => state.errorResponse
  );
}
