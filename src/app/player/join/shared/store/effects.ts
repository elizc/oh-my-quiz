import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '@shared/store';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Team, TeamService } from '@shared/team';
import { Observable, of } from 'rxjs';
import { catchError, exhaustMap, filter, map, tap, withLatestFrom } from 'rxjs/operators';
import { PlayerJoinPageActions } from '@app/player/join/shared/store/actions';
import { PlayerJoinPageSelectors } from '@app/player/join/shared/store/selectors';
import { Router } from '@angular/router';
import { PlayerActions, PlayerSelectors } from '@shared/player';

@Injectable()
export class PlayerJoinPageEffects {
  public submitForm$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerJoinPageActions.submitForm),
      withLatestFrom(
        this.store.select(PlayerJoinPageSelectors.formState),
        this.store.select(PlayerSelectors.game)
      ),
      filter(([_, formState]) => formState.isValid),
      map(([_, formState, gameFromState]) => {
        const team = new Team({
          code: gameFromState.code,
          name: formState.value.teamName,
          players: formState.value.players.filter((player) => player !== '')
        });

        return PlayerJoinPageActions.create({ team: team });
      })
    )
  );

  public create$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerJoinPageActions.create),
      exhaustMap((action) => {
        return this.teamService
          .create(action.team)
          .pipe(
            map((team) => PlayerJoinPageActions.createSuccess({ team })),
            catchError((response) => of(PlayerJoinPageActions.createFailure({ response })))
          );
      })
    )
  );

  public createSuccess$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
      ofType(PlayerJoinPageActions.createSuccess),
      map((action) => PlayerActions.join({ team: action.team })),
      tap(() => this.router.navigate(['/player/game']))
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private teamService: TeamService,
    private router: Router
  ) { }
}
