export class PlayerJoinPageForm {
  public teamName: string;
  public players: Array<string>;
}
