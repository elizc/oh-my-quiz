import { PlayerJoinPageComponent } from './join.component';
import { PlayerJoinPageEffects, playerJoinPageReducer } from './shared/store';
import { AppState } from '@shared/store/state';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { render, RenderResult } from '@testing-library/angular';
import { configuration } from '@configuration';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('PlayerJoinPageComponent', () => {
  let component: RenderResult<PlayerJoinPageComponent>;
  let componentInstance: PlayerJoinPageComponent;
  let store: Store<AppState>;

  const translation = require(`../../../assets/i18n/${configuration.languages.default}.json`);

  beforeEach(async () => {
    component = await render(PlayerJoinPageComponent, {
      imports: [
        TranslateTestingModule.withTranslations(configuration.languages.default, translation),
        StoreModule.forRoot({}, {
          runtimeChecks: {
            strictStateImmutability: true,
            strictActionImmutability: true
          }
        }),
        StoreModule.forFeature('playerJoinPage', playerJoinPageReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([PlayerJoinPageEffects])
      ],
      declarations: [
        PlayerJoinPageComponent
      ],
      routes: [],
      providers: []
    });

    componentInstance = component.fixture.componentInstance;
    store = TestBed.inject(Store);
  });

  it('should create', async () => {
    expect(component).toBeDefined();
  });
});

