import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlayerJoinPageComponent } from './join.component';
import { PlayerJoinPageRoutingModule } from './join.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { playerJoinPageReducer, PlayerJoinPageEffects } from './shared/store';
import { NgrxFormsModule } from 'ngrx-forms';

@NgModule({
  declarations: [
    PlayerJoinPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    PlayerJoinPageRoutingModule,
    StoreModule.forFeature('playerJoinPage', playerJoinPageReducer),
    EffectsModule.forFeature([PlayerJoinPageEffects]),
    NgrxFormsModule
  ],
  providers: []
})
export class PlayerJoinPageModule { }
