import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Quiz } from '@shared/quiz';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerSelectors } from '@shared/player';

@Component({
  selector: 'app-player-root',
  templateUrl: 'player.html',
  styleUrls: ['player.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent {
  public quiz$: Observable<Quiz>;

  constructor(private store: Store<AppState>) {
    this.quiz$ = this.store.select(PlayerSelectors.quiz);
  }
}
