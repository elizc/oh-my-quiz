import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlayerComponent } from './player.component';
import { PlayerRoutingModule } from './player.routing';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PlayerComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PlayerRoutingModule
  ]
})
export class PlayerModule { }
