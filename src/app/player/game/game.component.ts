import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerActions, PlayerSelectors } from '@shared/player';
import { GameStage } from '@shared/game';

@Component({
  selector: 'player-game-page',
  templateUrl: 'game.html',
  styleUrls: ['game.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGamePageComponent implements OnInit {
  public stage$: Observable<GameStage>;
  public gameStage: typeof GameStage;

  constructor(private store: Store<AppState>) {
    this.stage$ = this.store.select(PlayerSelectors.stage);
    this.gameStage = GameStage;
  }

  public ngOnInit(): void {
    this.store.dispatch(PlayerActions.init());
  }
}
