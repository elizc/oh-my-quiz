import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlayerGamePageComponent } from './game.component';
import { PlayerGamePageRoutingModule } from './game.routing';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { playerGamePageReducer, PlayerGamePageEffects } from './shared/store';
import { PlayerGameWaitComponent } from '@app/player/game/shared/components/wait/wait.component';
import { PlayerGameReadyComponent } from '@app/player/game/shared/components/ready/ready.component';
import { PlayerGameQuestionComponent } from '@app/player/game/shared/components/question/question.component';
import { PlayerGameQuestionResultsComponent } from '@app/player/game/shared/components/question-results/question-results.component';
import { PlayerGameFinalResultsComponent } from '@app/player/game/shared/components/final-results/final-results.component';

@NgModule({
  declarations: [
    PlayerGamePageComponent,
    PlayerGameWaitComponent,
    PlayerGameReadyComponent,
    PlayerGameQuestionComponent,
    PlayerGameQuestionResultsComponent,
    PlayerGameFinalResultsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    PlayerGamePageRoutingModule,
    StoreModule.forFeature('playerGamePage', playerGamePageReducer),
    EffectsModule.forFeature([PlayerGamePageEffects])
  ],
  providers: []
})
export class PlayerGamePageModule { }
