import { AppState } from '@shared/store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { PlayerGamePageState } from './state';
import { Team } from '@shared/team';

const selectFeature = (state: AppState) => state.playerGamePage;

export class PlayerGamePageSelectors {

}
