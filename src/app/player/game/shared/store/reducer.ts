import { Action, createReducer, on } from '@ngrx/store';
import { PlayerGamePageActions } from './actions';
import { PlayerGamePageState } from './state';

const initialState = new PlayerGamePageState();

const reducer = createReducer(
  initialState,
  on(PlayerGamePageActions.resetState, () => initialState)
);

export function playerGamePageReducer(state: PlayerGamePageState | undefined, action: Action): PlayerGamePageState {
  return reducer(state, action);
}
