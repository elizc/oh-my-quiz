import { createAction } from '@ngrx/store';

export class PlayerGamePageActions {
  /* tslint:disable:typedef */
  public static resetState = createAction(
    '[PlayerGamePage] Reset State'
  );
  /* tslint:enable:typedef */
}
