import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerActions, PlayerSelectors } from '@shared/player';

@Component({
  selector: 'final-results',
  templateUrl: 'final-results.html',
  styleUrls: ['final-results.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGameFinalResultsComponent {
  public teams$: Observable<Array<Team>>;
  public quiz$: Observable<Quiz>;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PlayerSelectors.teams);
    this.quiz$ = this.store.select(PlayerSelectors.quiz);
  }

  public quit(): void {
    this.store.dispatch(PlayerActions.quit());
  }
}
