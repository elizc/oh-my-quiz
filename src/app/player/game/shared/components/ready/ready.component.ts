import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerSelectors } from '@shared/player';

@Component({
  selector: 'ready',
  templateUrl: 'ready.html',
  styleUrls: ['ready.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGameReadyComponent {
  public getReadyTimer$: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.getReadyTimer$ = this.store.select(PlayerSelectors.getReadyTimer);
  }
}
