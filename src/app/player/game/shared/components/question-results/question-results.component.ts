import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Quiz } from '@shared/quiz';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerSelectors } from '@shared/player';

@Component({
  selector: 'question-results',
  templateUrl: 'question-results.html',
  styleUrls: ['question-results.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGameQuestionResultsComponent {
  public teams$: Observable<Array<Team>>;
  public quiz$: Observable<Quiz>;
  public questionsCount$: Observable<number>;
  public currentQuestionNumber$: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PlayerSelectors.teams);
    this.quiz$ = this.store.select(PlayerSelectors.quiz);
    this.questionsCount$ = this.store.select(PlayerSelectors.questionsCount);
    this.currentQuestionNumber$ = this.store.select(PlayerSelectors.currentQuestionNumber);
  }
}
