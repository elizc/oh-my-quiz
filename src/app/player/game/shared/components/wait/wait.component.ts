import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '@shared/team';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerSelectors } from '@shared/player';
import { Quiz } from '@shared/quiz';

@Component({
  selector: 'wait',
  templateUrl: 'wait.html',
  styleUrls: ['wait.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGameWaitComponent  {
  public teams$: Observable<Array<Team>>;
  public quiz$: Observable<Quiz>;

  constructor(private store: Store<AppState>) {
    this.teams$ = this.store.select(PlayerSelectors.teams);
    this.quiz$ = this.store.select(PlayerSelectors.quiz);
  }
}
