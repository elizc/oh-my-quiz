import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Question } from '@shared/question';
import { Store } from '@ngrx/store';
import { AppState } from '@shared/store';
import { PlayerSelectors, PlayerActions } from '@shared/player';

@Component({
  selector: 'question',
  templateUrl: 'question.html',
  styleUrls: ['question.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerGameQuestionComponent {
  public question$: Observable<Question>;
  public selectedAnswer$: Observable<string>;
  public questionTimer$: Observable<number>;
  public formattedQuestionTimer$: Observable<string>;
  public questionsCount$: Observable<number>;
  public currentQuestionNumber$: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.question$ = this.store.select(PlayerSelectors.question);
    this.selectedAnswer$ = this.store.select(PlayerSelectors.selectedAnswer);
    this.questionsCount$ = this.store.select(PlayerSelectors.questionsCount);
    this.currentQuestionNumber$ = this.store.select(PlayerSelectors.currentQuestionNumber);
    this.questionTimer$ = this.store.select(PlayerSelectors.questionTimer);
    this.formattedQuestionTimer$ = this.store.select(PlayerSelectors.formattedQuestionTimer);
  }

  public selectAnswer(answer: string): void {
    this.store.dispatch(PlayerActions.sendAnswer({ selectedAnswerName: answer }));
  }
}
