import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedGuard, UnauthenticatedGuard } from '@shared/auth';
import { HasPlayerGameGuard } from '@shared/player';
import { HasPresenterGameGuard } from '@shared/presenter';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./public/public.module').then((module) => module.PublicModule),
    canActivate: [UnauthenticatedGuard]
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then((module) => module.AccountModule),
    canActivate: [AuthenticatedGuard]
  },
  {
    path: 'player',
    loadChildren: () => import('./player/player.module').then((module) => module.PlayerModule),
    canActivate: [UnauthenticatedGuard, HasPlayerGameGuard]
  },
  {
    path: 'presenter',
    loadChildren: () => import('./presenter/presenter.module').then((module) => module.PresenterModule),
    canActivate: [AuthenticatedGuard, HasPresenterGameGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
