import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ApiModule } from '@shared/api';
import { AuthEffects, AuthModule, authReducer, jwtOptionsFactory } from '@shared/auth';
import { UserEffects, UserModule, userReducer } from '@shared/user';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { LanguageEffects, LanguageModule } from '@shared/language';
import { NotificationEffects, NotificationModule } from '@shared/notification';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalEffects, ModalModule } from '@shared/modal';
import { MediaModule } from '@shared/media';
import { EchoModule } from '@shared/echo';
import { GameModule } from '@shared/game/game.module';
import { PresenterEffects, presenterReducer } from '@shared/presenter';
import { TeamModule } from '@shared/team';
import { PlayerEffects, PlayerModule, playerReducer } from '@shared/player';
import { AnswerModule } from '@shared/answer';
import { PresenterModule } from '@shared/presenter/presenter.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Store]
      }
    }),
    UserModule,
    ApiModule,
    AuthModule,
    AppRoutingModule,
    LanguageModule,
    NotificationModule,
    ModalModule,
    MediaModule,
    GameModule,
    EchoModule,
    TeamModule,
    AnswerModule,
    PlayerModule,
    PresenterModule,
    ToastrModule.forRoot(),
    EffectsModule.forRoot([AuthEffects, UserEffects, LanguageEffects, NotificationEffects, ModalEffects, PresenterEffects, PlayerEffects]),
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot({
      router: routerReducer,
      authState: authReducer,
      userState: userReducer,
      presenter: presenterReducer,
      player: playerReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 30,
      logOnly: false
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
