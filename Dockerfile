FROM webdevops/nginx:ubuntu-16.04

WORKDIR /app
COPY dist/oh-my-quiz-angular /app

ENV WEB_DOCUMENT_ROOT /app
ENV WEB_DOCUMENT_INDEX index.html
